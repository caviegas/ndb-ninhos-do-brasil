<?php

namespace Drupal\br_address_field\Plugin\Field\FieldFormatter;

use Drupal\Component\Utility\Html;
use Drupal\Core\Field\FieldItemInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'br_address_plain_formatter' formatter.
 *
 * @FieldFormatter(
 *   id = "br_address_plain_formatter",
 *   label = @Translation("Brazilian address"),
 *   field_types = {
 *     "br_address_field_type"
 *   }
 * )
 */
class BrAddressPlainFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'display_state' => '',
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $display_state_options = [
      0 => $this->t('Complete name'),
      1 => $this->t('Initials'),
    ];

    $element['display_state'] = [
      '#title' => $this->t('How to show state field'),
      '#type' => 'select',
      '#default_value' => 1,
      '#options' => $display_state_options,
    ];

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];

    $display_state_options = [
      0 => $this->t('Complete name'),
      1 => $this->t('Initials'),
    ];

    $summary[] = $this->t('How to show state field: @style', ['@style' => $display_state_options[$this->getSetting('display_state')]]);
    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];

    $states = [
      'AC' => 'Acre',
      'AL' => 'Alagoas',
      'AP' => 'Amapá',
      'AM' => 'Amazonas',
      'BA' => 'Bahia',
      'CE' => 'Ceará',
      'DF' => 'Distrito Federal',
      'ES' => 'Espírito Santo',
      'GO' => 'Goiás',
      'MA' => 'Maranhão',
      'MT' => 'Mato Grosso',
      'MS' => 'Mato Grosso do Sul',
      'MG' => 'Minas Gerais',
      'PA' => 'Pará',
      'PB' => 'Paraíba',
      'PR' => 'Paraná',
      'PE' => 'Pernambuco',
      'PI' => 'Piauí',
      'RJ' => 'Rio de Janeiro',
      'RN' => 'Rio Grande do Norte',
      'RS' => 'Rio Grande do Sul',
      'RO' => 'Rondônia',
      'RR' => 'Roraima',
      'SC' => 'Santa Catarina',
      'SP' => 'São Paulo',
      'SE' => 'Sergipe',
      'TO' => 'Tocantins',
    ];

    foreach ($items as $delta => $item) {
      $values = $item->getValue();

      switch ($this->getSetting('display_state')) {
        case 0:
          $state = $states[$values['state']];
          break;

        case 1:
          $state = $values['state'];
          break;
      }

      $elements[$delta] = [
        '#markup' => $this->viewValue($item),
        '#theme' => 'br_address_field',
        '#postal_code' => $values['postal_code'],
        '#thoroughfare' => $values['thoroughfare'],
        '#number' => $values['number'],
        '#street_complement' => $values['street_complement'],
        '#neighborhood' => $values['neighborhood'],
        '#city' => $values['city'],
        '#state' => $state,
      ];
    }

    return $elements;
  }

  /**
   * Generate the output appropriate for one field item.
   *
   * @param \Drupal\Core\Field\FieldItemInterface $item
   *   One field item.
   *
   * @return string
   *   The textual output generated.
   */
  protected function viewValue(FieldItemInterface $item) {
    // The text value has no text format assigned to it, so the user input
    // should equal the output, including newlines.
    return nl2br(Html::escape($item->value));
  }

}
