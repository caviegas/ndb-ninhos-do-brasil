<?php

namespace Drupal\ln_datalayer\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\Yaml\Yaml;

/**
 * Defines a generic controller for Datalayer.
 *
 * @package Drupal\ln_datalayer\Controller
 */
class LnDatalayerController extends ControllerBase {

  /**
   * Return an array with the available countries keyed by acronym.
   *
   * @return array|mixed
   *   return array.
   */
  public static function getCountries() {
    $countries = drupal_get_path('module', 'ln_datalayer') . '/datasets/countries.yml';
    $countriesList = file_exists($countries) ? Yaml::parseFile($countries) : [];
    return $countriesList;
  }

  /**
   * Return an array with the available site zones keyed by acronym.
   *
   * @return array|mixed
   *   return array.
   */
  public static function getZones() {
    $zones = drupal_get_path('module', 'ln_datalayer') . '/datasets/zones.yml';
    $zonesList = file_exists($zones) ? Yaml::parseFile($zones) : [];
    return $zonesList;
  }

  /**
   * Return an array with the available site properties keyed by acronym.
   *
   * @return array|mixed
   *   return array.
   */
  public static function getProperties() {
    $properties = drupal_get_path('module', 'ln_datalayer') . '/datasets/properties.yml';
    $propertiesList = file_exists($properties) ? Yaml::parseFile($properties) : [];
    return $propertiesList;
  }

  /**
   * Return an array with the available site technologies/cms keyed by acronym.
   *
   * @return array|mixed
   *   return array.
   */
  public static function getTechnologies() {
    $technologies = drupal_get_path('module', 'ln_datalayer') . '/datasets/technologies.yml';
    $technologiesList = file_exists($technologies) ? Yaml::parseFile($technologies) : [];
    return $technologiesList;
  }

  /**
   * Return an array with the available site target audiences keyed by acronym.
   *
   * @return array|mixed
   *   return array.
   */
  public static function getTargetAudiences() {
    $targetAudiences = drupal_get_path('module', 'ln_datalayer') . '/datasets/target_audiences.yml';
    $targetAudiencesList = file_exists($targetAudiences) ? Yaml::parseFile($targetAudiences) : [];
    return $targetAudiencesList;
  }

  /**
   * Return an array with the available site property types keyed by acronym.
   *
   * @return array|mixed
   *   return array.
   */
  public static function getProperteyTypes() {
    $propertyTyepes = drupal_get_path('module', 'ln_datalayer') . '/datasets/property_types.yml';
    $propertyTyepesList = file_exists($propertyTyepes) ? Yaml::parseFile($propertyTyepes) : [];
    return $propertyTyepesList;
  }

  /**
   * Return an array with the available businesses keyed by acronym.
   *
   * @return array|mixed
   *   return array.
   */
  public static function getBusinesses() {
    $propertyTyepes = drupal_get_path('module', 'ln_datalayer') . '/datasets/businesses.yml';
    $propertyTyepesList = file_exists($propertyTyepes) ? Yaml::parseFile($propertyTyepes) : [];
    return $propertyTyepesList;
  }

  /**
   * Return an array with the available businesses keyed by acronym.
   *
   * @return array|mixed
   *   return array.
   */
  public static function getlanguages() {
    $languages = drupal_get_path('module', 'ln_datalayer') . '/datasets/languages.yml';
    $languagesList = file_exists($languages) ? Yaml::parseFile($languages) : [];
    return $languagesList;
  }

  /**
   * Return an array with the available brand keyed by acronym.
   *
   * @return array|mixed
   *   return array.
   */
  public static function getBrands() {
    $brands = drupal_get_path('module', 'ln_datalayer') . '/datasets/brands.yml';
    $brandsList = file_exists($brands) ? Yaml::parseFile($brands) : [];
    return $brandsList;
  }

  /**
   * Return an array with the available sub_brand keyed by acronym.
   *
   * @return array|mixed
   *   return array.
   */
  public static function getSubBrands() {
    $sub_brands = drupal_get_path('module', 'ln_datalayer') . '/datasets/sub_brands.yml';
    $subBrandsList = file_exists($sub_brands) ? Yaml::parseFile($sub_brands) : [];
    return $subBrandsList;
  }

  /**
   * Return an array with the available property_status keyed by acronym.
   *
   * @return array|mixed
   *   return array.
   */
  public static function getPropertyStatus() {
    $sub_brands = drupal_get_path('module', 'ln_datalayer') . '/datasets/property_status.yml';
    $subBrandsList = file_exists($sub_brands) ? Yaml::parseFile($sub_brands) : [];
    return $subBrandsList;
  }

  /**
   * Return the value for a key from array.
   *
   * @param string $key
   *   Key string.
   * @param string $filename
   *   Filename string.
   *
   * @return mixed|string
   *   return strng.
   */
  public static function getSelectedValue($key, $filename) {
    $file = drupal_get_path('module', 'ln_datalayer') . '/datasets/' . $filename . '.yml';
    $fileData = file_exists($file) ? Yaml::parseFile($file) : [];
    return ($fileData && isset($fileData[$key])) ? $fileData[$key] : '';
  }

}
