CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Functionality
 * Troubleshooting
 * Extend
 * Maintainers

INTRODUCTION
------------

Get page meta data from inside Drupal out to the client-side.

This Drupal module outputs various CMS page meta data (like content type, author uid, taxonomy terms), which can be used for all kinds of front-end features. This works for all entity types and is easy to extend with hooks.

The phase "lightnest data layer" is a Google term, but it's a great standard for your server to setup a foundation for the front-end. 
It's generic enough that other services managed in GTM can use application data, also you can use this data on your site to implement great client-side features, like anonymous user tracking, etc.


REQUIREMENTS
------------

This module requires the following modules:

* dataLayer (https://www.drupal.org/project/datalayer)

INSTALLATION
------------

* Install as you would normally install a contributed Drupal module. Visit
   https://www.drupal.org/node/1897420 for further information.


CONFIGURATION
-------------

* Configutaion URLs **/admin/config/lightnest/ln-datalayer/lightnest-datalayer**.

FUNCTIONALITY
-------------

* Add more component json in header.

TROUBLESHOOTING
---------------

 * If the content does not display in page view source, check the following:

   - Check hook_datalayer_alter hook in ln_datalayer.module file.

EXTEND
------

 * hook_datalayer_alter for new data.

MAINTAINERS
-----------

* Nestle Webcms team.