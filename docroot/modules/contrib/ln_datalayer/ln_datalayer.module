<?php

/**
 * @file
 * Provides primary Drupal hook implementations.
 */

use Drupal\user\Entity\User;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\RedirectCommand;
use Drupal\Core\Url;
use Drupal\Core\Ajax\ReplaceCommand;
use Drupal\datalayer\Ajax\PushCommand;
use Drupal\ln_datalayer\Controller\LnDatalayerController;

/**
 * Implements hook_help().
 */
function ln_datalayer_help($route_name, RouteMatchInterface $route_match) {
  switch ($route_name) {
    // Main module help for the tint module.
    case 'help.page.ln_datalayer':
      $output = '';
      $output .= '<h3>' . t('About') . '</h3>';
      $output .= '<p>' . t('Lightnest Datalyer Integration for GMT.') . '</p>';

      return $output;

    default:
  }
}

/**
 * Get saved form settings and replace with dynamic for current page.
 *
 * @param array $data_layer
 *   Datalayer alter the existing.
 */
function ln_datalayer_datalayer_alter(array &$data_layer) {

  // Datalayer is not required on user entity for security purpose.
  if (isset($data_layer['entityType']) && $data_layer['entityType'] == 'user') {
    $data_layer = [];
    return;
  }
  // Unset not required  dataset from datalayer module.
  if (isset($data_layer['drupalLanguage']) || isset($data_layer['drupalCountry']) || isset($data_layer['pageName']) || isset($data_layer['userUid'])) {
    unset($data_layer['drupalLanguage']);
    unset($data_layer['drupalCountry']);
    unset($data_layer['pageName']);
    unset($data_layer['userUid']);
    unset($data_layer['siteName']);
  }

  // System form config values.
  $config = \Drupal::config('ln_datalayer.settings');
  $brand = $config->get('brand');
  $sub_brand = $config->get('sub_brand');
  $digiPiID = $config->get('digi_pi_id');
  $zone = $config->get('zone');
  $country = $config->get('country');
  $business = $config->get('business');
  $properties = $config->get('properties');
  $siteType = $config->get('siteType');
  $technology = $config->get('technology');
  $language = $config->get('language');
  $property_status = $config->get('property_status');

  // Node type and cookies values for getting GA ID.
  $nodeType = $pageName = '';
  $gaClientID = '';
  $request_cookies = \Drupal::request()->cookies;
  if (is_object($request_cookies) && $request_cookies->get('_ga')) {
    $gaID = $request_cookies->get('_ga');
    $gaClientID = $gaID;
  }
  // Validation for node landing page attributes.
  $request = \Drupal::request()->attributes->get('node');
  if ($request != NULL && is_object($request)) {
    $nodeType = \Drupal::request()->attributes->get('node')->bundle();
    $pageName = \Drupal::request()->attributes->get('node')->getTitle();
  }

  // Detect device and add it on json for header.
  $device = detect_device();
  $data_layer['event'] = 'fireTags';
  $data_layer['pageInformation'] = [
    'pageCategory' => $nodeType,
    'pageName' => $pageName,
    'pageType' => $nodeType,
    'pageSubsection' => ($config->get('page_subsection')) ? $config->get('page_subsection') : NULL,
    'pageSection' => $nodeType,
  ];
  $lang_code = \Drupal::LanguageManager()->getCurrentLanguage()->getId();
  // Get all settings config form values.
  $status = \Drupal::requestStack()
    ->getCurrentRequest()->attributes->get('exception');
  $date = ($config->get('go_live_date')) ? new DrupalDateTime($config->get('go_live_date'), 'UTC') : '';
  $goLiveDate = (is_object($date)) ? $date->format('d/m/Y') : '';
  $data_layer['siteInformation'] = [
    'zone' => !empty($zone) ? LnDatalayerController::getSelectedValue($zone, 'zones') : NULL,
    'country' => !empty($country) ? LnDatalayerController::getSelectedValue($country, 'countries') : NULL,
    'business' => !empty($business) ? LnDatalayerController::getSelectedValue($business, 'businesses') : NULL,
    'brand' => !empty($brand) ? LnDatalayerController::getSelectedValue($brand, 'brands') : NULL,
    'subBrand' => !empty($sub_brand) ? LnDatalayerController::getSelectedValue($sub_brand, 'sub_brands') : NULL,
    'language' => !empty($language) ? LnDatalayerController::getSelectedValue($language, 'languages') : NULL,
    'technology' => !empty($technology) ? LnDatalayerController::getSelectedValue($technology, 'technologies') : NULL,
    'properties' => !empty($properties) ? LnDatalayerController::getSelectedValue($properties, 'properties') : NULL,
    'siteType' => !empty($siteType) ? LnDatalayerController::getSelectedValue($siteType, 'property_types') : NULL,
    'targetAudience' => ($config->get('target_audience')) ? LnDatalayerController::getSelectedValue($config->get('target_audience'), 'target_audiences') : NULL,
    'siteName' => !empty(\Drupal::config('system.site')
      ->get('name')) ? \Drupal::config('system.site')->get('name') : NULL,
    'businessUnit' => ($config->get('business_unit')) ? $config->get('business_unit') : NULL,
    'digiPiID' => !empty($digiPiID) ? $digiPiID : NULL,
    'conversionPageType' => ($config->get('conversion_page_type')) ? $config->get('conversion_page_type') : NULL,
    'websiteUrl' => ($config->get('website_url_' . $lang_code)) ? $config->get('website_url_' . $lang_code) : NULL,
    'statusHttps' => ($status) ? (string) $status->getStatusCode() : '200',
    'propertyStatus' => !empty($property_status) ? LnDatalayerController::getSelectedValue($property_status, 'property_status') : NULL,
    'goLiveDate' => $goLiveDate,
    'siteCategory' => ($config->get('site_category')) ? $config->get('site_category') : NULL,
    'videoType' => ($config->get('video_type')) ? $config->get('video_type') : NULL,
  ];
  $data_layer['doubleClickData'] = [
    'floodlightAdvertiserID' => ($config->get('advertiser_id')) ? $config->get('advertiser_id') : NULL,
    'hasEcommerce' => ($config->get('ecommerce')) ? TRUE : FALSE,
    'hasLogin' => ($config->get('login')) ? TRUE : FALSE,
    'hasSignup' => ($config->get('sign_up')) ? TRUE : FALSE,
    'hasCouponPrint' => ($config->get('coupon_print')) ? TRUE : FALSE,
    'hasCheckout' => ($config->get('checkout')) ? TRUE : FALSE,
    'conversionPageType' => ($config->get('conversion_page_type')) ? $config->get('conversion_page_type') : NULL,
    'countGroupTagString' => ($config->get('count_group_tag_string')) ? $config->get('count_group_tag_string') : NULL,
    'salesGroupTagString' => ($config->get('sales_group_tag_string')) ? $config->get('sales_group_tag_string') : NULL,
  ];

  $currentAccount = \Drupal::currentUser();
  // Set user information with another json structure for header.
  $data_layer['userInformation'] = [
    'deviceType' => $device,
    'userID_Hit' =>'ID'.$currentAccount->id(),
    'loginStatus' => ($currentAccount->id() != 0) ? true : false,
    'gaClientID' => !empty($gaClientID) ? $gaClientID : NULL,
  ];
}

/**
 * Get current device name and type.
 */
function detect_device() {
  $userAgent = $_SERVER["HTTP_USER_AGENT"];
  // Define type of devices for comparing with current one.
  $devicesTypes = [
    "desktop" => [
      "msie 10",
      "msie 9",
      "msie 8",
      "windows.*firefox",
      "windows.*chrome",
      "x11.*chrome",
      "x11.*firefox",
      "macintosh.*chrome",
      "macintosh.*firefox",
      "opera",
    ],
    "tablet" => ["tablet", "android", "ipad", "tablet.*firefox"],
    "mobile" => [
      "mobile ",
      "android.*mobile",
      "iphone",
      "ipod",
      "opera mobi",
      "opera mini",
    ],
  ];

  // Looping to find matched device.
  foreach ($devicesTypes as $deviceType => $devices) {
    foreach ($devices as $device) {
      if (preg_match("/" . $device . "/i", $userAgent)) {
        $deviceName = $deviceType;
      }
    }
  }
  // Return with device name.
  return !empty($deviceName) ? ucfirst($deviceName) : '';
}

/**
 * Alter the Data Layer data before it is output to the screen.
 *
 * @param array $properties
 *   Data layer properties to output on entiity pages.
 */
function ln_datalayer_datalayer_meta_alter(array $properties) {
  // Remove author uid if anonymous or admin.
  if (array_key_exists('uid', $properties)) {
    if ($properties['uid'] == 0 || $properties['uid'] == 1) {
      unset($properties['uid']);
    }
  }
}

/**
 * Implements hook_preprocess_HOOK() for node.html.twig.
 */
function ln_datalayer_preprocess_node(&$variables) {
  $variables['#attached']['library'][] = 'ln_datalayer/datalayer-library';
}

/**
 * Get list actions by form_id to use ajax.
 *
 * @param mixed $form_id
 *
 * @return array
 */

/**
 * Implements hook_forms().
 */
function getlist_forms($form_id) {
  $configs = \Drupal::config('ln_datalayer.settings');
  $data = $configs->getRawData();
  $list = [];
  if (count($data) > 0) {
    for ($x = 0; $x < count($data); $x++) {
      $value = explode(':', $configs->get('ln_datalayer_' . $x));
      if (in_array($form_id, $value)) {
        $list[] = $configs->get('ln_datalayer_' . $x);
      }
    }
  }

  return $list;
}

/**
 * Implements hook_form_alter().
 */
function ln_datalayer_form_alter(&$form, FormStateInterface &$form_state, $form_id) {
  $list = getlist_forms($form_id);
  if (count($list) > 0) {
    $selector = "ajaxify_submit_form_" . $form_id;
    $form['#prefix'] = '<div id="' . $selector . '">';
    $form['#suffix'] = '</div>';
    foreach ($list as $value) {
      $form['status_messages_' . $form_id] = [
        '#type' => 'status_messages',
        '#weight' => -10,
      ];
      $form['form_id'] = [
        '#type' => 'hidden',
        '#value' => $form_id,
      ];
      $value = explode(':', $value);

      $action = $value[1];
      // Adjust the form to use ajax submit.
      $form['actions'][$action]['#ajax'] = [
        'callback' => 'ajaxify_submit_forms_form_ajax_callback',
        'wrapper' => $selector,
        'effect' => 'fade',
        '#attributes' => [
          'class' => [
            'use-ajax',
          ],
        ],
      ];
    }
  }
}

/**
 * Define a validation for forms.
 */
function ajaxify_submit_forms_form_ajax_callback(&$form, FormStateInterface &$form_state) {
  $response = new AjaxResponse();
  $form_id = $form_state->getUserInput()['form_id'];
  if ($form_state->hasAnyErrors()) {
    $wrapper_id = '#ajaxify_submit_form_' . $form_id;
    ln_datalayer_form_ajax_validate($wrapper_id, $form, $form_id, $response, $form_state);
  }
  else {
    ln_datalayer_form_ajax_response($form_id, $response, $form_state);
  }
  return $response;
}

/**
 * Define responses .
 */
function ln_datalayer_form_ajax_response($form_id, $response, &$form_state) {
  switch ($form_id) {
    case 'user_login_form':
      $current_user = \Drupal::currentUser();
      $uid = $current_user->id();
      $user = User::load($uid);
      $user_type = ($user->get('field_user_type')->value == 1) ? 'nestle_employee' : 'agency';
      $response->addCommand(new PushCommand([
        'event' => 'userLogin',
        'eventCategory' => 'account',
        'eventAction' => 'login-successful',
        'eventLabel' => 'email',
        'accountType' => $user_type,
        'loginStatus' => 'yes',
        'loginType' => 'email',
        'userID' => $current_user->id(),
        'userID_User' => 'ID'.$current_user->id(),
        'userType' => $user_type,
        'login-completed' => 1,
        'login-completed:email' => 1,
      ]));

      $response->addCommand(new PushCommand([
        'event' => 'userLogin',
        'eventCategory' => 'account',
        'eventAction' => 'completionScreen',
        'eventLabel' => 'email',
        'login-error' => 1,

      ]));
      $destination = \Drupal::request()->query->get('destination');
      $redirect_url = (!is_null($destination) && !empty($destination)) ? '/' . $destination : (URL::fromRoute('<front>')
        ->toString());
      $response->addCommand(new RedirectCommand($redirect_url));
      break;

    case 'user_register_form':
      $user = user_load_by_name($form_state->getValue('name'));
      $user_type = ($user->get('field_user_type')->value == 1) ? 'nestle_employee' : 'agency';
      $response->addCommand(new PushCommand([
        'event' => 'userRegistration',
        'eventCategory' => 'form:registration',
        'eventAction' => 'completed_registration',
        'eventLabel' => 'email',
        'accountType' => $user_type,
        'userID' => $user->id(),
        'userID_User' => 'ID'.$user->id(),
        'registrationSubmitted' => 'email',
        'userType' => $user_type,
        'registrationSubmitted' => 1,
        'registrationSubmitted:email' => 1,
      ]));
      $dsu_security_config = \Drupal::config('dsu_security.settings');
      $redirect_url = (!empty($dsu_security_config->get('redirect_url'))) ? $dsu_security_config->get('redirect_url') : '/';
      $response->addCommand(new RedirectCommand($redirect_url, 302));
      break;
  }
  return $response;
}

/**
 * Define responses .
 */
function ln_datalayer_form_ajax_validate($wrapper_id, $form, $form_id, $response, FormStateInterface &$form_state) {
  switch ($form_id) {
    case 'user_login_form':
      if ($form_state->hasAnyErrors()) {
        $errors = $form_state->getErrors();
        // We only remove messages if we get the following error.
        $reset_message = preg_grep("/(Unrecognized username or password)|(is already taken)./", $errors);
        if (count($reset_message)) {
          // Resets the form error status so no form fields are highlighted in
          // red.
          $form_state->setRebuild();
          $form_state->clearErrors();

          // Removes "$name is not recognized as a username or an email.".
          \Drupal::messenger()->deleteAll();
          $response->addCommand(new PushCommand([
            'event' => 'userLogin',
            'eventCategory' => 'account',
            'eventAction' => 'login-error',
            'eventLabel' => 'email',
            'login-error' => 1,
          ]));
          $response->addCommand(new PushCommand([
            'event' => 'userLogin',
            'eventCategory' => 'account',
            'eventAction' => 'login-started:email',
            'eventLabel' => 'email',
            'login-started' => 1,
            'login-started:email' => 1,
          ]));
          $response->addCommand(new ReplaceCommand($wrapper_id, $form));
        }
        else {
          $response->addCommand(new ReplaceCommand($wrapper_id, $form));
        }
      }
      break;

    case 'user_register_form':
      if ($form_state->hasAnyErrors()) {
        // Validate user already exist by email. Try to load by email.
        $account = user_load_by_mail($form_state->getValue('mail'));
        if (empty($account)) {
          // No success, try to load by name.
          $account = user_load_by_name($form_state->getValue('name'));
        }
        if ($account && \Drupal::currentUser()
          ->isAnonymous() && $account->id() != 0) {
          $form_state->setRebuild();
          $form_state->clearErrors();
          \Drupal::messenger()->deleteAll();
          // Send email to user.
          $mail = _user_mail_notify('status_exist', $account);
          // Resets the form error status so no form fields are highlighted in.
          // red.
          if (!empty($mail)) {
            \Drupal::messenger()->addMessage(t('An e-mail has been sent with further instructions.'), 'status');
          }
          $dsu_security_config = \Drupal::config('dsu_security.settings');
          $redirect_url = (!empty($dsu_security_config->get('redirect_url'))) ? $dsu_security_config->get('redirect_url') : '/';
          $response->addCommand(new RedirectCommand($redirect_url, 301));
        }
        $response->addCommand(new ReplaceCommand($wrapper_id, $form));

      }
      else {
        $response->addCommand(new ReplaceCommand($wrapper_id, $form));
      }
      break;
  }
  return $response;
}
