<?php

namespace Drupal\block_registration_status\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\node\Entity\Node;
use Drupal\Core\Entity\Query\QueryInterface;
Use \Drupal\taxonomy\Entity\Term;
use Drupal\Core\Entity\EntityTypeManager;
use Drupal\user\Entity\User;
use Drupal\views\Entity\View;

/**
 * Provides a 'Feed' Block.
 *
 * @Block(
 *   id = "block_registration_status",
 *   admin_label = "Registration status block",
 *   category = "Meeg blocks",
 * )
 */
class RegistrationStatusBlock extends BlockBase {
  /**
   * {@inheritdoc}
   */
  public function build() {

    $logged_in = \Drupal::currentUser()->isAuthenticated();

    // Apenas se estiver logado
    if ($logged_in) {
    /* 
      Verificar se o usuário possui a sessão padrão get_user q. Se tiver, com certeza 
      ele terá a sessão get_attribute pois são criadas nas informações básicas de registro 
      pela API. Então pegaremos os valores desta última.

      Essa verificação serve para saber se é um usuário cadastrado pela API 
      ou um usuário comum criado apenas no drupal.
    */
      if (isset($_SESSION['get_user'])) {
        $userData = $_SESSION['get_attributes'];
      } else {
        $userData = [
          'id_estado_civil' => 'Solteiro',
          // ['nm_filho_01' => 'Silvia'],
          'id_sexo' => 'Silvia',
          // ['nu_cep' => 'Silvia'],
          // 'nm_tags' => 'Silvia',
        ];
      };

      $url = null;
      $percentage = 25;
      $family = 0; //max 2
      $detail = 0; //max 2
      $interests = 0; //max 1
      foreach ($userData as $key => $value) {
        switch ($key) {
          // FAMILY
          case  'id_estado_civil';
            if ($value != '0') {
              $percentage += 10;
              $family++;
            }
            break;
          case 'nm_filho_01';
            $percentage += 15;
            $family++;
            break;
          // DETAIL
          case 'id_sexo';
            $percentage += 10;
            $detail++;
            break;
          case 'nu_cep';
            $percentage += 15;
            $detail++;
            break;
          // INTERESTS
          case 'nm_tags';
            $percentage += 25;
            $interests++;
            break;
        }
      }

      if ($family != 2) {
        $url = '/familia';
      } elseif ($detail != 2) {
        $url = '/detalhamento';
      } else {
        $url = '/interesses';
      }

      $status = [
        'percentage' => $percentage,
        'url' => $url
      ];
      
      return [
        '#theme' => 'registration_status',
        '#status' => $status
      ];
    }
  }
}
