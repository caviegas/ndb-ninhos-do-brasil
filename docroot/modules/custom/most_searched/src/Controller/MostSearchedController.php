<?php

namespace Drupal\most_searched\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Defines LabController class.
 */
class MostSearchedController extends ControllerBase {

 
    public function sum() {
     
        // $database = \Drupal::database();

        // $keys = 'doce';

        // $qidObj = $database
        // ->select('most_searched', 'ms')
        // ->condition('q', $keys)
        // ->fields('ms', ['qid', 'q', 'counter'])
        // // ->fields('ms')
        // ->execute()->fetch();
    
        // if ($qidObj) {            
        //     $res = $database->update('most_searched')
        //     ->condition('qid', $qidObj->qid)
        //     ->fields([
        //         'counter' => $qidObj->counter + 1,
        //     ])
        //     ->execute();
        // } else {
        //     $res = 'no';
        // }
        //   \Drupal::cache('render')->deleteAll();
          \Drupal::service('cache.render')->invalidateAll();
          $res = 'cache cleared';

        
        return new JsonResponse($res);
    }
}