<?php

namespace Drupal\analytics_api\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Defines ConnectController class.
 */
class ConnectController extends ControllerBase {
    
    public function topViewsUpdate(Request $request) {
        
        $database = \Drupal::database();
        $data = json_decode($request->getContent());

        // If table is empty, create records.
        $result = $database->select('analytics__top_views')->countQuery()->execute()->fetchField();
        $empty = !$result;

        if ($empty) {
            for ($i = 0; $i < count($data); ++$i) {
                $query = $database->insert('analytics__top_views')
                ->fields([
                    'nid' => $data[$i]->nid,
                    'views' => $data[$i]->views,
                    'created' => \Drupal::time()->getRequestTime()
                ]);
                $query->execute();
            }
            $message = new JsonResponse(['message' => 'Success: Criado com sucesso']);
            $message->setStatusCode(200);
        } else {
            try {
                // Update
                for ($i = 0; $i < count($data); $i++) {
                    $query = $database->update('analytics__top_views')
                    ->condition('id', $i+1)
                    // ->condition('nid', $record->nid, '!=')
                    ->fields([
                        'nid' => $data[$i]->nid,
                        'views' => $data[$i]->views
                    ]);
                    $query->execute();
                }
                \Drupal::logger('NDB Analytics - Top Views [teste]')->notice('Conteúdos mais visualizados atualizados!');
                $message = new JsonResponse(['message' => 'Success: Atualizado com sucesso']);
                $message->setStatusCode(200);
            } catch(\Throwable $e) {
                \Drupal::logger('NDB Analytics - Top Views [teste]')->notice($e->getMessage());
                $message = new JsonResponse(['message' => 'Failed: '.$e]);
                $message->setStatusCode(500);
            }
        }

        return $message;
    }
    // TODO: caso a gente mande menos registro do que já existe, ele não acrescentará. Por exemplo, se ele achar UM registro, ele vai desconsiderar a primeira condição. Entrando na segunda, ele vai apenas substituir os existentes
}