<?php

namespace Drupal\analytics_api\Services;

use Drupal\Core\Database\Connection;
use Drupal\Core\Session\AccountProxy;
use Symfony\Component\HttpFoundation\RequestStack;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Logger\LoggerChannelFactory;
use Drupal\Component\Datetime\TimeInterface;
use Symfony\Component\HttpFoundation;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Service handler for Notification Logger.
 */
class ApiConnectionService {

  protected $currentUser;
  protected $database;
  protected $time;

  /**
   * {@inheritdoc}
   */
  public function __construct(AccountProxy $current_user, Connection $database, TimeInterface $time) 
  {
    $this->currentUser   = $current_user;
    $this->database      = $database;
    $this->time          = $time;
  }


  public function logger($data) {

    try {
      $client = \Drupal::httpClient();
      $request = $client->request('POST', 'https://ninho.meeg.app/api/logger', $data);

      $response = 'Succeed!';
      $error = false;
    } catch (\Throwable $e) {

      $response = 'Failed!';
      $error = true;
    } finally {
      if ($error) {
        // $database = \Drupal::database();
        $this->database->insert('leftover__logger')
        ->fields($data['json'])
        ->execute();
        \Drupal::logger('NDB Analytics - Logger')->error($e->getMessage());
      }
    }
        
    return new JsonResponse(['message' => $response]);
  }

  public function pageview($data) {

    try {
      $client = \Drupal::httpClient();
      $request = $client->request(
        'POST', 
        'https://ninho.meeg.app/api/pageview', 
        $data
      );

      $error = false;
    } catch (\Throwable $e) {
      $error = true;
    } finally {
      if ($error) {
        // $database = \Drupal::database();
  
        $this->database->insert('leftover__pageview')
        ->fields(
          $data['json']
        )
        ->execute();

        \Drupal::logger('NDB Analytics - Pageview')->error($e->getMessage());
      }
    }

    return;
  }

  private function urlCleaning($url) {

    $url = "https://www.ninhosdobrasil.com.br".$url;

    // Remover "/" final
    $url = preg_replace('/\/$/', '', $url);
    // Remover "pt-br" 
    $url = preg_replace('/\/pt-br/', '', $url);
    // Remover /node que as vezes aparece na home (remove apenas que a url terminar em /node)
    $url = preg_replace('/\/node$/', '', $url);
  
    // Remover string ?destination= em diante
    if(strpos($url, "?destination=") == true){
      $url = preg_replace('/\?destination=.*$/', '', $url);
    }
  
    // Remover paginação do fim
    $url = preg_replace('/[\&|\?]page\=([0-9])*$/', '', $url);

    return $url;
  }
}