<?php

/**
 * @file
 * Contains captain_hook.module.
 */

use \Drupal\user\UserInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Url;
use Drupal\block_content\Controller\UserController;
use Drupal\block_content\Controller\ContentController;

/*
* Implements hook
*/

// Hook para Conteúdos lidos
function captain_hook_node_view(array &$build, \Drupal\Core\Entity\EntityInterface $entity, \Drupal\Core\Entity\Display\EntityViewDisplayInterface $display, $view_mode) {

  $contentController = new ContentController;
  $userController = new UserController;

  // Verifica se usuário autenticado
  if (\Drupal::currentUser()->isAuthenticated() && $userController->hasBasicData() && $view_mode == 'full' && !$entity->in_preview) {

    // Pois não precisamos salvar conteúdos lidos de admins...
    // Ah, e nem anuncios e tópicos
    if (!in_array($entity->bundle(), ['ad', 'page'])) {
      // Coletar informações necessárias do conteúdo acessado e [supostamente] lido      
      $content = new \stdClass();
      $content->id = $entity->id();
      $content->slug = Url::fromRoute('entity.node.canonical', ['node' => $entity->id()])->toString();
      $content->title = $entity->getTitle();;
      $content->date = date('d/m/Y');

      $contentController->verifyContent($content);
    }
  }
  
  return;
}


// Notificações de aprovações em fórum
function captain_hook_entity_update(\Drupal\Core\Entity\EntityInterface $entity) {
  $notificationService = \Drupal::service('notifications_widget.logger');

  // Notificar criação de tópico aprovada
  if ($entity->bundle() == 'forum' && $entity->isPublished()) {
    $message = [];
    $message['id'] = $entity->id();
    $message['entity_nid'] = $entity->id();
    $message['bundle']  = 'forum';
    // $message['content'] = "Seu tópico <i>".$node->getTitle()."</i> recebeu um novo comentário";
    $message['content'] = $entity->getTitle();
    $message['content_link'] = $entity->toUrl()->toString().'#comment-'.$entity->id();

    $notificationService->logNotifyTopicApproval($message, 'topic_approval', $entity, $entity->getOwnerId());
  }

  // Notificações de comentário aprovado
  if ($entity->bundle() == 'comment_forum' && $entity->isPublished()) {

    $node = $entity->getCommentedEntity();

    // Notificar o autor do post se não for o mesmo dono do comentário
    if ($node->getOwnerId() != $entity->getOwnerId()) {
        $message = [];
        $message['id'] = $entity->id();
        $message['entity_nid'] = $node->id();
        $message['bundle']  = 'comment_forum';
        // $message['content'] = "Seu tópico <i>".$node->getTitle()."</i> recebeu um novo comentário";
        $message['content'] = $node->getTitle();
        $message['content_link'] = $node->toUrl()->toString().'#comment-'.$entity->id();
    
        $notificationService->logNotifyAuthor($message, 'author', $entity, $node->getOwnerId());
    }

    /* 
        Notificar todos os usuários que também comentaram neste topico 
        Exceto o dono do comentário atual
    */

    // Pega o ID de todos os comentários neste post
    $cids = \Drupal::entityQuery('comment')
    ->condition('comment_type', 'comment_forum')
    ->condition('entity_id', $node->id())
    // ->condition('entity_id', 2)
    ->sort('cid', 'DESC')
    ->execute();

    // Se houver comentários
    if ($cids) {
      // Carrega esses comentários como objetos
      $comments = \Drupal::entityTypeManager()
        ->getStorage('comment')
        ->loadMultiple($cids);

      // A partir dos objetos, extrair o ID dos autores
      $commenterIds;
      foreach($comments as $c) {
        $commenterIds[] = $c->getOwnerId();
      }
      // Elimina os repetidos
      $commenterIds = array_unique($commenterIds);

      foreach ($commenterIds as $commenterId) {
        if ($commenterId != $entity->getOwnerID()){
          $message = [];
          $message['id'] = $entity->id();
          $message['entity_nid'] = $node->id();
          $message['bundle']  = 'comment_forum';
          $message['content'] = $node->getTitle();
          $message['content_link'] = $node->toUrl()->toString().'#comment-'.$entity->id();
      
          $notificationService->logNotifyCommenters($message, 'commenter', $entity, $commenterId);
        } else {
          $message = [];
          $message['id'] = $entity->id();
          $message['entity_nid'] = $node->id();
          $message['bundle']  = 'comment_forum';
          $message['content'] = $node->getTitle();
          $message['content_link'] = $node->toUrl()->toString().'#comment-'.$entity->id();
      
          $notificationService->logNotifyCommentApproval($message, 'comment_approval', $entity, $commenterId);
        }
      }
    } 
  }
  return;
}

// Only admins
function captain_hook_comment_insert(Drupal\Core\Entity\EntityInterface $entity) {
  
  $current_user_roles = \Drupal::currentUser()->getRoles();
  if (!in_array('administrator', $current_user_roles)) {
    return;
  }
  
  $node = $entity->getCommentedEntity();
  $notificationService = \Drupal::service('notifications_widget.logger');

  
  // Notificar o autor do post se não for o mesmo dono do comentário
  if ($node->getOwnerId() != $entity->getOwnerId()) {
      $message = [];
      $message['id'] = $entity->id();
      $message['entity_nid'] = $node->id();
      $message['bundle']  = 'comment_forum';
      $message['content'] = $node->getTitle();
      $message['content_link'] = $node->toUrl()->toString().'#comment-'.$entity->id();
  
      $notificationService->logNotifyAuthor($message, 'author', $entity, $node->getOwnerId());
  }

  /* 
      Notificar todos os usuários que também comentaram neste topico 
      Exceto o dono do comentário atual
  */

  // Pega o ID de todos os comentários neste post
  $cids = \Drupal::entityQuery('comment')
  ->condition('comment_type', 'comment_forum')
  ->condition('entity_id', $node->id())
  // ->condition('entity_id', 2)
  ->sort('cid', 'DESC')
  ->execute();

  // Se houver comentários
  if ($cids) {
    // Carrega esses comentários como objetos
    $comments = \Drupal::entityTypeManager()
      ->getStorage('comment')
      ->loadMultiple($cids);

    // A partir dos objetos, extrair o ID dos autores
    $commenterIds;
    foreach($comments as $c) {
      $commenterIds[] = $c->getOwnerId();
    }
    // Elimina os repetidos
    $commenterIds = array_unique($commenterIds);

    foreach ($commenterIds as $commenterId) {
      if ($commenterId != $entity->getOwnerID()){
        $message = [];
        $message['id'] = $entity->id();
        $message['entity_nid'] = $node->id();
        $message['bundle']  = 'comment_forum';
        $message['content'] = $node->getTitle();
        $message['content_link'] = $node->toUrl()->toString().'#comment-'.$entity->id();
    
        $notificationService->logNotifyCommenters($message, 'commenter', $entity, $commenterId);
      }
    }
  } 

  return;
}
