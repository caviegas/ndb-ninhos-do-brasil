<?php
/**
 * @file
 * Contains \Drupal\trialmachine_campaign\Controller\CampaignController
 */

 namespace Drupal\module_nestle_upload\Controller;

 use Drupal\Core\Controller\ControllerBase;
 use Drupal\Core\Datetime\DrupalDateTime;
 use Drupal\trialmachine_campaign\Entity\Campaign;
 use Drupal\Core\Session\AccountInterface;
 use Symfony\Component\HttpFoundation\Request;
 use Symfony\Component\HttpFoundation\RedirectResponse;
 use Symfony\Component\HttpFoundation\Response;

 use Drupal\module_nestle_upload\Entity\UsersUploads;

 class IndexController extends ControllerBase
 {
    public $session;

    public function __construct() {

    }



    public function uploadSuccess() {
 
      return [
        '#theme' => 'upload-success',
        '#data' => $this->t('Test Value'),
      ];
   
    }


    public function uploads() {
      $nids = \Drupal::entityQuery('users_uploads')
      // ->condition('approved', 0)
      ->condition('approved', 1)
      ->execute();
      $data = UsersUploads::loadMultiple($nids);

      $build = [
        '#theme' => 'uploads',
        '#data' => $data,

      ];
  
      return $build;
   
    }
    

 
 }

