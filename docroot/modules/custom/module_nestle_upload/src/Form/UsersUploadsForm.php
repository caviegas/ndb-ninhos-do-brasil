<?php
/**
 * @file
 * Contains \Drupal\module_nestle_upload\Form\UsersUploadsForm.
 */

namespace Drupal\module_nestle_upload\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Form controller for the content_entity_example entity edit forms.
 *
 * @ingroup content_entity_example
 */
class UsersUploadsForm extends ContentEntityForm {

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    /* @var $entity \Drupal\module_nestle_upload\Entity\UsersUploads */
    $form = parent::buildForm($form, $form_state);
    
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    // dd($form_state->getValues());
    // Redirect to term list after save.
    $form_state->setRedirect('entity.users_uploads.collection');
    $entity = $this->getEntity();
    $entity->save();
  }
}
