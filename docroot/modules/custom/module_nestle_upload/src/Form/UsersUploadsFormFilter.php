<?php

namespace Drupal\module_nestle_upload\Form\UsersUploads;


use Drupal\Core\Form;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\HtmlCommand;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Datetime\DrupalDateTime;

use Drupal\Core\Url;
use Drupal\Core\Messenger;
use \SoapClient;
use \SoapVar;
use \SoapHeader;
use \SoapFault;
use Symfony\Component\HttpFoundation\RedirectResponse;

use Drupal\Core\Database\Connection;



/**
 * Our custom ajax form.
 */
class UsersUploadsFormFilter extends FormBase
{
    /**
     * {@inheritdoc}
     */
    public function getFormId()
    {
        return "form_users_uploads_filter";
    }

    /**
     * {@inheritdoc} 
     */

    public function buildForm(array $form, FormStateInterface $form_state) {

     
      $request = \Drupal::request();
    
      $form['filter'] = [
        '#type' => 'container',
        '#attributes' => [
          'class' => ['form--inline', 'clearfix'],
        ],
      ];
    
    //   $form['filter']['user_id'] = [
    //     '#type' => 'entity_autocomplete',
    //     '#target_type' => 'trialmachine_campaign',
    //     '#title' => $this->t('Campanha'),
    //     '#description' => $this->t('Selecione uma campanha para filtrar os cupons.'),
    //     // '#default_value' => $default_entities,
    //     '#tags' => TRUE,
    //     '#weight' => '0',
    // ];
    
      
      $form['actions']['wrapper'] = [
        '#type' => 'container',
        '#attributes' => ['class' => ['form-item']],
      ];
      $form['actions']['wrapper']['submit'] = [
        '#type' => 'submit',
        '#value' => 'Filter',
      ];
    
      if ($request->getQueryString()) {
        $form['actions']['wrapper']['reset'] = [
          '#type' => 'submit',
          '#value' => 'Reset',
          '#submit' => ['::resetForm'],
        ];
      }

      return $form;
    }

    public function submitForm(array &$form, FormStateInterface $form_state) {
      $query = [];
    
    //   $foo = $form_state->getValue('campaign_id') ?? 0;
    //   if ($foo) {
    //     $query['campaign_id'] = $foo;
    //   }
      
      $form_state->setRedirect('entity.module_nestle_upload.collection', $query);
    }
    
    public function resetForm(array $form, FormStateInterface &$form_state) {
      $form_state->setRedirect('entity.module_nestle_upload.collection');
    }
}