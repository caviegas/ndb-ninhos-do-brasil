<?php

namespace Drupal\module_nestle_upload\Form;
use Drupal\Core\Form;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\HtmlCommand;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Url;
use Drupal\Core\Messenger;
use Drupal\user;
use \SoapClient;
use \SoapVar;
use \SoapHeader;
use \SoapFault;


use Drupal\user_location\Controller\UserLocationController;
use Symfony\Component\HttpFoundation\RedirectResponse;

use Drupal\file\Entity\File;

use Drupal\module_nestle_upload\Entity\UsersUploads;


/**
 * Our custom ajax form.
 */
class formUploadImage extends FormBase {

  
  /**
   * {@inheritdoc}
   */
  public function getFormId() {
     return "form_upload_image";
   }

   /**
    * {@inheritdoc}
    */

  public function buildForm(array $form, FormStateInterface $form_state) {
    /**
     * Campos do formulário
     */
    if (\Drupal::currentUser()->isAuthenticated()) {
 
    }else{
      return new RedirectResponse('/uploadImage');  
    }

  
    $form['my_file'] = array(
      '#type' => 'managed_file',
      '#name' => 'my_file',
      '#title' => t('Upload a new member photo'),
      '#title_display' => 'invisible',
      // '#size' => 20,
      '#multiple' => FALSE,
      '#description' => t('PDF format only'),
      '#upload_validators' => [
        'file_validate_extensions' => ['png jpg jpeg'],
        'file_validate_size' => [25600000],
      ],
      // '#progress_indicator' => 'bar',
        '#progress_message' => 'Wait ...',
        // '#theme' => 'image_testform',
        // '#theme' => 'image_widget',
        '#preview' => TRUE,
      // '#preview_image_style' => 'thumbnail',
      '#preview_image_style' => 'medium',
      '#styles' => TRUE,
      '#upload_location' => 'public://my_files/',
      '#required' => TRUE
    );

    // $form['image_with_preview'] = [
    //   '#type' => 'managed_file',
    //   '#title' => t('Image with preview'),
    //   '#upload_validators' => [
    //     'file_validate_extensions' => ['gif png jpg jpeg'],
    //     'file_validate_size' => [25600000],
    //   ],
    //   // '#theme' => 'image_widget',
    //   // '#preview_image_style' => 'medium',
    //   '#widget' => 'imce',
    //   // Add an additional "select" element with image styles. When this is TRUE,
    //   // then return value will be an array with two properties: "fid" and "style"
    //   // instead of usual file ID as a string.
    //   '#styles' => TRUE,
    //   // Append a preview of the uploaded file.
    //   '#preview' => TRUE,
    //   '#upload_location' => 'public://',
    //   '#required' => FALSE,
    // ];

    $form['description'] = [
      '#type' => 'textarea',
      '#title' => 'Descrição da imagem',
      '#attributes' => array(
        'autocomplete' => 'off',
      ),
    ];

    $form['terms'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Aceito os <a href="recuperar" class="forgot-link">termos de uso</a>'),
    ];

    $form['message'] = [
      '#type' => 'markup',
      '#markup' => '<div class="result_message"></div>'
    ]; 

    $form['actions']['#type'] = 'actions';
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Entrar'),
      '#button_type' => 'primary',
    ];
    
    return $form;
   }

   public function validateForm(array &$form, FormStateInterface $form_state){

    if ($form_state->getValue('my_file') == NULL) {
      $form_state->setErrorByName('my_file', "Você precisa adicionar uma foto.");
    }

    if($form_state->getValue('description') == '') {
      $form_state->setErrorByName('description', "Você precisa incluir uma descrição para a foto.");
    }

   }



   
  public function submitForm(array &$form, FormStateInterface $form_state) {


    $event_image = $form_state->getValue('my_file');
  if ($event_image) {
    $file = File::load(reset($event_image));
    $file->setPermanent();
    
    

    $usersUploads = UsersUploads::create([
      'image' => $file->id(),
      'user_id' => \Drupal::currentUser()->id(),
      'description' => $form_state->getValue('description'),
      'terms' => TRUE,
      'approved' => FALSE,
      'category' => '-'
    ]);
    $usersUploads->save();


    $file->save();
    drupal_set_message('Filename: ' . $file->id());
  }




    $redirect_path = "/uploadImageSuccess";
    $url = url::fromUserInput($redirect_path);
    $form_state->setRedirectUrl($url);
    


    return "Ok";
  }



}
