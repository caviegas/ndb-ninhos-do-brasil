<?php
/**
 * @file
 * Contains \Drupal\content_entity_example\Entity\ContentEntityExample.
 */

namespace Drupal\module_nestle_upload\Entity;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\user\UserInterface;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Datetime\DrupalDateTime;

/**
 * Defines the ContentEntityExample entity.
 *
 * @ingroup usersUploads
 *
 *
 * @ContentEntityType(
 *   id = "users_uploads",
 *   label = @Translation("Users uploads"),
 *   list_cache_contexts = { "users_uploads" },
 *   base_table = "users_uploads",
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\module_nestle_upload\Entity\Controller\UsersUploadsListBuilder",
 *     "form" = {
 *       "add" = "Drupal\module_nestle_upload\Form\UsersUploadsForm",
 *       "edit" = "Drupal\module_nestle_upload\Form\UsersUploadsForm",
 *       "delete" = "Drupal\module_nestle_upload\Form\CouponDeleteForm",
 *     },
 *     "access" = "Drupal\module_nestle_upload\UsersUploadsAccessControlHandler",
 *   },
 *   entity_keys = {
 *     "id" = "id",
 *     "terms" = "title",
 *     "terms_date" = "summary",
 *     "description" = "description",
 *     "image__target_id" = "image__target_id",
 *     "image__alt" = "image__alt",
 *     "image__title" = "image__title",
 *     "image__width" = "image__width",
 *     "image__height" = "image__height",
 *     "approved" = "approved",
 *     "category" = "category",
 *     "created" = "created",
 *    },
 *    links = {
 *     "canonical" = "/module_nestle_upload/{users_uploads}",
 *     "edit-form" = "/module_nestle_upload/{users_uploads}/edit",
 *     "delete-form" = "/module_nestle_upload/{users_uploads}/delete",
 *     "collection" = "/users_uploads/list"
 *   },
 *   field_ui_base_route = "entity.user_uploads.settings",
 *   common_reference_target = TRUE,
 * )
 */
class UsersUploads extends ContentEntityBase {

  use EntityChangedTrait;


  /**
   * {@inheritdoc}
   *
   * When a new entity instance is added, set the user_id entity reference to
   * the current user as the creator of the instance.
   */
  public static function preCreate(EntityStorageInterface $storage_controller, array &$values) {
    parent::preCreate($storage_controller, $values);
    // Default author to current user.
    $values += array(
      'user_id' => \Drupal::currentUser()->id(),
    );
  }

  /**
   * {@inheritdoc}
   *
   * Define the field properties here.
   *
   * Field name, type and size determine the table structure.
   *
   * In addition, we can define how the field and its content can be manipulated
   * in the GUI. The behaviour of the widgets used can be determined here.
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {

    // Standard field, used as unique if primary index.
    $fields['id'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('ID'))
      ->setDescription(t('The ID of the Campaign entity.'))
      ->setReadOnly(TRUE);

    // Standard field, unique outside of the scope of the current project.
    $fields['image'] = BaseFieldDefinition::create('image')
      ->setLabel(t('Imagem'))
      ->setDescription(t('Imagem da campanha.'))
      ->setRevisionable(TRUE)
      ->setTranslatable(TRUE)
      ->setDisplayOptions('view', [
        'type' => 'image',
        'weight' => 2,
        'label' => 'hidden',
        'settings' => [
          'image_style' => 'large',
        ],
      ])
      ->setDisplayOptions('form',  [
        'type' => 'image',
        'weight' => 20,
        'label' => 'hidden',
        'settings' => [
          'image_style' => 'large',
        ]
      ])
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayConfigurable('form', TRUE)
      ->setReadOnly(TRUE);

    $fields['description'] = BaseFieldDefinition::create('string_long')
    ->setLabel(t('Descrição'))
    ->setDescription(t('Descrição da campanha.'))
    ->setRequired(TRUE)
    ->setSettings([
      'default_value' => '',
      'max_length' => 255,
      'text_processing' => 0,
      'readonly' => true
    ])
    ->setDisplayOptions('view', [
      'label' => 'hidden',
      'type' => 'text_textfield',
      'weight' => 0,
      'readonly' => true
    ])
    ->setDisplayOptions('form', [
      'label' => 'above',
      'type' => 'text_textfield',
      'weight' => 0,
      'readonly' => true
    ])
    ->setDisplayConfigurable('form', TRUE)
    ->setDisplayConfigurable('view', TRUE);


    $fields['approved'] = BaseFieldDefinition::create('boolean')
    ->setLabel(t('Aprovada'))
    ->setDescription(t('Aprovada'))
    ->setRequired(FALSE)
    ->setDisplayOptions('view', [
      'type' => 'checkbox',
      'weight' => 2,
      'label' => 'hidden',
    ])
    ->setDisplayOptions('form',  [
      'type' => 'checkbox',
      'weight' => 2,
      'label' => 'hidden',
    ])
    ->setDisplayConfigurable('view', TRUE)
    ->setDisplayConfigurable('form', TRUE)
    ->setReadOnly(TRUE);


    $fields['category'] = BaseFieldDefinition::create('string')
    ->setLabel(t('Categoria'))
    ->setDescription(t('.'))
    ->setSettings([
      'default_value' => '',
      'max_length' => 255,
      'text_processing' => 0
    ])
    ->setDisplayOptions('view', [
      'label' => 'hidden',
      'type' => 'text_textfield',
      'weight' => 0,
    ])
    ->setDisplayOptions('form', [
      'label' => 'above',
      'type' => 'text_textfield',
      'weight' => 0,
    ])
    ->setDisplayConfigurable('form', TRUE)
    ->setDisplayConfigurable('view', TRUE)
    ->setRequired(FALSE);

    

    // Owner field of the contact.
    // Entity reference field, holds the reference to the user object.
    // The view shows the user name field of the user.
    // The form presents a auto complete field for the user name.
    $fields['user_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Autor'))
      ->setDescription(t('O usuário que fez o upload.'))
      ->setSetting('target_type', 'user')
      ->setSetting('handler', 'default')
      ->setDisplayOptions('view', array(
        'label' => 'hidden',
        'type' => 'author',
        'weight' => 3,
      ))
      ->setDisplayOptions('form', array(
        'label' => 'above',
        'type' => 'entity_reference_autocomplete',
        'settings' => array(
          'match_operator' => 'CONTAINS',
          'size' => 60,
          'placeholder' => '',
        ),
        'weight' => 3,
      ))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);


      $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the entity was created.'));






    return $fields;
  }

}
