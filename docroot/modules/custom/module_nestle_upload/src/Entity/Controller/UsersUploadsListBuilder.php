<?php

/**
 * @file
 * Contains \Drupal\module_nestle_upload\Entity\Controller\TermListBuilder.
 */

namespace Drupal\module_nestle_upload\Entity\Controller;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Routing\UrlGeneratorInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;


/**
 * Provides a list controller for module_nestle_upload entity.
 *
 * @ingroup coupon
 */
class UsersUploadsListBuilder extends EntityListBuilder {

  /**
   * The url generator.
   *
   * @var \Drupal\Core\Routing\UrlGeneratorInterface
   */
  protected $urlGenerator;


  /**
   * {@inheritdoc}
   */
  public static function createInstance(ContainerInterface $container, EntityTypeInterface $entity_type) {
    // $value = \Drupal::request()->get('campaign_id');
    // dd($container->get('entity.manager')->getStorage($entity_type->id()));
    return new static(
      $entity_type,
      $container->get('entity_type.manager')->getStorage($entity_type->id()),
      $container->get('url_generator')
    );
  }

  /**
   * Constructs a new couponTermListBuilder object.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type term.
   * @param \Drupal\Core\Entity\EntityStorageInterface $storage
   *   The entity storage class.
   * @param \Drupal\Core\Routing\UrlGeneratorInterface $url_generator
   *   The url generator.
   */
  public function __construct(EntityTypeInterface $entity_type, EntityStorageInterface $storage, UrlGeneratorInterface $url_generator) {
    parent::__construct($entity_type, $storage);
    $this->urlGenerator = $url_generator;
  }


  protected function getEntityIds() {
    $query = \Drupal::entityQuery($this->entityTypeId);
    $request = \Drupal::request();
  
    // $users_uploads = $request->get('users_uploads') ?? 0;
    // if ($users_uploads) {
    //   $query->condition('users_uploads', $users_uploads[0]['target_id']);
    // }
  
    if ($this->limit) {
      $query->pager($this->limit);
    }
  
    return $query->execute();
  }

  /**
   * {@inheritdoc}
   *
   * We override ::render() so that we can add our own content above the table.
   * parent::render() is where EntityListBuilder creates the table using our
   * buildHeader() and buildRow() implementations.
   */
  public function render() {
    // $build['description'] = array(
    //   '#markup' => $this->t('Content Entity Example implements a couponTerms model. These are fieldable entities. You can manage the fields on the <a href="@adminlink">Term admin page</a>.', array(
    //     '@adminlink' => $this->urlGenerator->generateFromRoute('entity.coupon.term_settings'),
    //   )),
    // );


    // $build['form'] = \Drupal::formBuilder()->getForm('Drupal\module_nestle_upload\Form\UsersUploadsFormFilter');

    $build['table'] = parent::render();


    return $build;
  }

  /**
   * {@inheritdoc}
   *
   * Building the header and content lines for the trialmachine_coupon list.
   *
   * Calling the parent::buildHeader() adds a column for the possible actions
   * and inserts the 'edit' and 'delete' links as defined for the entity type.
   */
  public function buildHeader() {
    $header['id'] = $this->t('ID');
    // $header['image'] = $this->t('Foto');
    $header['user_id'] = $this->t('User');
    $header['description'] = $this->t('Descrição');
    $header['category'] = $this->t('Categoria');
    $header['approved'] = $this->t('Aprovada');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /* @var $entity \Drupal\module_nestle_upload\Entity\UsersUploads */
    $row['id'] = $entity->id();
    // $row['image'] = $entity->image->entity ? file_create_url($entity->image->entity->getFileUri()) : '';
    $row['user_id'] = $entity->user_id->entity->name->value;
    $row['description'] = $entity->description->value;
    $row['category'] = $entity->category->value;
    $row['approved'] = $entity->approved->value;
    return $row + parent::buildRow($entity);
  }

}
