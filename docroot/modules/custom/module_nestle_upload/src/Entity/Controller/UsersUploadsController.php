<?php
/**
 * @file
 * Contains \Drupal\trialmachine_campaign\Controller\CampaignController
 */

 namespace Drupal\module_nestle_upload\Controller;

 use Drupal\Core\Controller\ControllerBase;
 use Drupal\Core\Datetime\DrupalDateTime;
 use Drupal\trialmachine_campaign\Entity\Campaign;
 use Drupal\Core\Session\AccountInterface;
 use Symfony\Component\HttpFoundation\Request;
 use Symfony\Component\HttpFoundation\RedirectResponse;


 class UsersUploadsController extends ControllerBase
 {
    public $session;

    public function __construct() {

    }



    public function uploadSuccess() {
 
      return [
        '#theme' => 'my_template',
        '#test_var' => $this->t('Test Value'),
      ];
   
    }


    public function uploads() {
 
      return [
        '#theme' => 'uploads',
        '#test_var' => $this->t('Test Value'),
      ];
   
    }
    

 
 }

