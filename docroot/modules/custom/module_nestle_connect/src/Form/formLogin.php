<?php

namespace Drupal\module_nestle_connect\Form;
use Drupal\Core\Form;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\HtmlCommand;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Url;
use Drupal\Core\Messenger;
use Drupal\user;
use \SoapClient;
use \SoapVar;
use \SoapHeader;
use \SoapFault;
use Drupal\block_content\Controller\UserController;
use Drupal\module_nestle_connect\Controller\SessionController;
use Drupal\user_location\Controller\UserLocationController;
use Symfony\Component\HttpFoundation\RedirectResponse;


/**
 * Our custom ajax form.
 */
class formLogin extends FormBase {

  
  /**
   * {@inheritdoc}
   */
  public function getFormId() {
     return "form_login";
   }

   /**
    * {@inheritdoc}
    */

  public function buildForm(array $form, FormStateInterface $form_state) {
    /**
     * Campos do formulário
     */
    if (\Drupal::currentUser()->isAuthenticated()) {
      return new RedirectResponse('/');   
    }

    $form['email'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Seu e-mail *'),
      '#attributes' => array(
        'autocomplete' => 'off',
      ),
    ];

    $form['senha1'] = [
      '#type' => 'password',
      '#title' => $this->t('Sua senha * <a href="recuperar" class="forgot-link">Esqueci minha senha</a>'),
      '#attributes' => array(
        'autocomplete' => 'off',
      ),
    ];




    // $form['captcha'] = array(
    //   '#type' => 'captcha',
    //   // '#captcha_type' => 'recaptcha/image_captcha',
    // );


    $form['message'] = [
      '#type' => 'markup',
      '#markup' => '<div class="result_message"></div>'
    ]; 

    $form['actions']['#type'] = 'actions';
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Entrar'),
      '#button_type' => 'primary',
    ];
    

     return $form;

   }

   public function validateForm(array &$form, FormStateInterface $form_state){

      if($form_state->getValues()['email'] == ''){
        $form_state->setErrorByName('email', 'Você precisa informar o seu e-mail.');
        $_SESSION['captcha_after']['form_login']++;
      }

      if (!filter_var($form_state->getValues()['email'], FILTER_VALIDATE_EMAIL)) {
        $form_state->setErrorByName('email', 'Você precisa digitar um e-mail válido.');
      }

      if($form_state->getValues()['senha1'] == ''){
        $form_state->setErrorByName('senha1', 'Você precisa digitar corretamente a sua senha.');
        $_SESSION['captcha_after']['form_login']++;
      }
   }



   
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $sessionController = new SessionController;
    $userController = new UserController;
    $userLocationController = new UserLocationController;

    $email =  $form_state->getValues()['email'];
        try {
          $data = [
            'username' => $form_state->getValues()['email'],
            'password' => $form_state->getValues()['senha1'],
          ];
          $client = new SoapClient($GLOBALS['CADU_API_URL'], array(
              'trace' => 1,
          ));
          $headerVar = new SoapVar('<PartnerCode xmlns="ns">16</PartnerCode><CryptoAreaSite xmlns="ns">TkRCTWVlZw==</CryptoAreaSite>',XSD_ANYXML);
          $header = new SoapHeader('http://tempuri.org/','RequestParams', $headerVar);                
          $client->__setSoapHeaders($header);
          $serviceStatus = $client->IsServiceAvailable();
          if ($serviceStatus != true) {
              $message = 'Serviço indisponível';
              return $message;
          }

          $res = $client->ValidateUser($data);
          $get_attributes = '';
          $get_attributes = $res->ValidateUserResult;
          $chk_status_login = 0;
          if($get_attributes->Status == 'InvalidLogin') {
            $res_attr = $client->GetAttributes(['userName' => $email]);
            // $attributes = $res_attr->GetAttributesResult->Atributo;
            if(property_exists($res_attr->GetAttributesResult, 'Atributo')){
              $_SESSION['captcha_after']['form_login']++;
              \Drupal::messenger()->addError('O login falhou.');
              $form_state->setRebuild();
            }else{
              \Drupal::messenger()->addError('O login falhou.');
              $form_state->setRebuild();
              // $message = 'E-mail não cadastrado. Deseja <a href="registrar">criar uma conta?</a>';
              // $rendered_message = \Drupal\Core\Render\Markup::create($message);
              //   \Drupal::messenger()->addError($rendered_message);
                $_SESSION['captcha_after']['form_login']++;
            }
              $form_state->disableRedirect();
              $chk_status_login = 1;
          }else if($get_attributes->Status == 'Blocked'){
            $form_state->disableRedirect();
            \Drupal::messenger()->addError('Sua conta foi temporariamente bloqueada após as suas tentativas de login.');
            $form_state->setRebuild();
          }else if($get_attributes->Status == 'Inactive'){
            $form_state->disableRedirect();
            \Drupal::messenger()->addError('Usuário inativo');
            $form_state->setRebuild();
          }else if($get_attributes->Status == 'Error'){
            $form_state->disableRedirect();
            \Drupal::messenger()->addError('Ocorreu um erro no login, tente novamente mais tarde');
            $form_state->setRebuild();
          }else if($get_attributes->Status == 'Unconfirmed'){
            $form_state->disableRedirect();
            \Drupal::messenger()->addError('O login falhou.');
            $form_state->setRebuild();
          }else{
                try {
                  $client = new SoapClient($GLOBALS['CADU_API_URL'], array(
                      'trace' => 1,
                  ));
                  // SET HEADERS
                  $headerVar = new SoapVar('<PartnerCode xmlns="ns">16</PartnerCode><CryptoAreaSite xmlns="ns">TkRCTWVlZw==</CryptoAreaSite>',XSD_ANYXML);
                  $header = new SoapHeader('http://tempuri.org/','RequestParams', $headerVar);                
                  $client->__setSoapHeaders($header);
                  
                  // Check if service is available
                  $serviceStatus = $client->IsServiceAvailable();
                  updateUserSession();             
              } catch (SoapFault $exception) {
                  echo '<pre>';
                  echo $exception->getMessage();
                  echo "\n REQUEST:\n" . htmlentities($client->__getLastRequest()) . "\n";    
              }

              try { 
                $client = new SoapClient($GLOBALS['CADU_API_URL'], array(
                    'trace' => 1,
                ));
                // SET HEADERS
                $headerVar = new SoapVar('<PartnerCode xmlns="ns">16</PartnerCode><CryptoAreaSite xmlns="ns">TkRCTWVlZw==</CryptoAreaSite>',XSD_ANYXML);
                $header = new SoapHeader('http://tempuri.org/','RequestParams', $headerVar);                
                $client->__setSoapHeaders($header);
                
                // Check if service is available
                $serviceStatus = $client->IsServiceAvailable();
                
                $res = $client->GetUser(['username' => $email]);
                
                $attr_user = [
                  "ativo" => $res->GetUserResult->Ativo,
                  "cpf" => $res->GetUserResult->CPF,
                  "cpf_responsavel" => $res->GetUserResult->CPFResponsavel,
                  "codigo" => $res->GetUserResult->Codigo,
                  "codigo_area_site_criacao" => $res->GetUserResult->CodigoAreaSiteCriacao,
                  "confirmacao_email" => $res->GetUserResult->ConfirmacaoEmail,
                  "data_acesso_bloquado" => $res->GetUserResult->DataAcessoBloqueado,
                  "data_alteracao" => $res->GetUserResult->DataAlteracao,
                  "data_criacao" => $res->GetUserResult->DataCriacao,
                  "data_nascimento" => $res->GetUserResult->DataNascimento,
                  "data_ultimo_login" => $res->GetUserResult->DataUltimoLogin,
                  "email" => $res->GetUserResult->Email,
                  "email_responsavel" => $res->GetUserResult->EmailResponsavel,
                  "expiration_date_token" => $res->GetUserResult->ExpirationDateToken,
                  "forgot_password_token" => $res->GetUserResult->ForgotPasswordToken,
                  "ip_criacao" => $res->GetUserResult->IPCriacao,
                  "login_result" => $res->GetUserResult->LoginResult,
                  "match_code_id" => $res->GetUserResult->MatchCodeId,
                  "nome" => $res->GetUserResult->Nome,
                  "nome_responsavel" => $res->GetUserResult->NomeResponsavel,
                  "quantidade_de_falhas_login" => $res->GetUserResult->QuantidadeDeFalhasLogin,
                  "questao_seguranca" => $res->GetUserResult->QuestaoSeguranca,
                  "resposta_questao_seguranca" => $res->GetUserResult->RespostaQuestaoSeguranca,
                  "senha" => $res->GetUserResult->Senha,
                  "tipo_cadastro" => $res->GetUserResult->Tipo_Cadastro,
                  "user_social_id" => $res->GetUserResult->UserSocialId

                ];

                $_SESSION["get_user"] = $attr_user;
    

                if($chk_status_login == 0){
                  unset($_SESSION['captcha_after']['form_login']);
                  $user = user_load_by_mail($form_state->getValues()['email']);
                
                  if($user){
                    $uid = $user->id();
                    $user = \Drupal\user\Entity\User::load($uid);         
                    $userDrupalCode = explode('-',$user->get('name')->value);
                    $code = end($userDrupalCode);
                    $user->set('name', $res->GetUserResult->Nome . "-" . $code);
                    $user->activate();
                    $user->save();
  
                    user_login_finalize($user);
                    
                    $message = '<script>var script = document.createElement("script"); script.innerHTML = "window.dataLayer = window.dataLayer || []; dataLayer.push({\'event\': \'Login Realizado\',\'visitorId\': \'' . hash('sha256', $form_state->getValues()['email']) . '\'})"; var head = document.getElementsByTagName("head")[0]; head.insertBefore(script, head.firstChild)</script>';
                    $rendered_message = \Drupal\Core\Render\Markup::create($message);
                    \Drupal::messenger()->addWarning(\Drupal\Core\Render\Markup::create($rendered_message));

                    \Drupal::messenger()->addMessage('Login efetuado com sucesso!');

                    updateUserSession();

                    $redirect_path = "/";
                    $url = url::fromUserInput($redirect_path);
                    $form_state->setRedirectUrl($url);
                  }else{
                      /**
                       * Caso o usuário tente logar com um login que só existe na API, cria usuário no Drupal
                       */
                      $nm_user_drupal = $res->GetUserResult->Nome;
                      $randon_user = bin2hex(openssl_random_pseudo_bytes(8));
                      $nm_user_drupal = $nm_user_drupal."-".$randon_user;
                      $language = \Drupal::languageManager()->getCurrentLanguage()->getId();
                      $user = \Drupal\user\Entity\User::create();
                      $user->setPassword($res->GetUserResult->Senha);
                      $user->enforceIsNew();
                      $user->setEmail($email);
                      $user->setUsername($nm_user_drupal);//This username must be unique and accept only a-Z,0-9, - _ @ .
                      $user->set("init", 'email');
                      $user->set("langcode", $language);
                      $user->set("preferred_langcode", $language);
                      $user->set("preferred_admin_langcode", $language);
                      $user->activate();
                      $user->save();
                      user_login_finalize($user);

                      $message = '<script>var script = document.createElement("script"); script.innerHTML = "window.dataLayer = window.dataLayer || []; dataLayer.push({\'event\': \'Login Realizado\',\'visitorId\': \'' . hash('sha256', $form_state->getValues()['email']) . '\'})"; var head = document.getElementsByTagName("head")[0]; head.insertBefore(script, head.firstChild)</script>';
                      $rendered_message = \Drupal\Core\Render\Markup::create($message);
                      \Drupal::messenger()->addWarning($rendered_message);
                      \Drupal::messenger()->addMessage('Login efetuado com sucesso!');
                      
                      updateUserSession();
                      $redirect_path = "/user/basico";
                      $url = url::fromUserInput($redirect_path);
                      $form_state->setRedirect('machine_name');
                      $form_state->setRedirectUrl($url);

                      // Criar localização do usuário no banco de dados, caso ele tiver:
                      $userAttr = $userController->getUserAttr();
                      if (isset($userAttr['dc_cidade']) && isset($userAttr['sg_estado'])) {
                        $data = [
                          'uuid' => $user->uuid(),
                          'city' => $userAttr['dc_cidade'],
                          'state' => $userAttr['sg_estado']
                        ];
                        $userLocationController->setUserLocation($data);
                      }
                  }
                }
            } catch (SoapFault $exception) {
                echo '<pre>';
                echo $exception->getMessage();
                echo "\n REQUEST:\n" . htmlentities($client->__getLastRequest()) . "\n";    
            }
          }      
        } catch (SoapFault $exception) {
          // $response = '<pre>'.$exception->getMessage()."\n REQUEST:\n" . htmlentities($client->__getLastRequest()) . "\n";    
          // \Drupal::messenger()->addError($response);
          // \Drupal::messenger()->addError($get_attributes);
          $form_state->disableRedirect();
          \Drupal::messenger()->addError('Login temporariamente indisponível');
          $form_state->setRebuild();
        }
      
    return "Ok";
  }



}
