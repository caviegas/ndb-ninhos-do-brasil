<?php

namespace Drupal\module_nestle_connect\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Session\AccountProxy;

/**
 * This is our hero controller.
 */
class IndexController extends ControllerBase {

  protected $configFactory;
  protected $currentUser;

  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('current_user')
    );
  }

  public function __construct(ConfigFactory $configFactory, AccountProxy $currentUser) {
    $this->configFactory = $configFactory;
    $this->currentUser = $currentUser;
  }

  public function welcome() {
    return "ok";
  }


}
