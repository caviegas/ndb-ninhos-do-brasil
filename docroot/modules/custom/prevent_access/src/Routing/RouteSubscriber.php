<?php

namespace Drupal\prevent_access\Routing;

use Drupal\Core\Routing\RouteSubscriberBase;
use Symfony\Component\Routing\RouteCollection;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Listens to the dynamic route events.
 */
class RouteSubscriber extends RouteSubscriberBase {

  /**
   * {@inheritdoc}
   */
  protected function alterRoutes(RouteCollection $collection) {

    // Edit user
    if ($route = $collection->get('entity.user.edit_form')) {
      // $route->setPath('/login');
      // $route->setRequirement('_permission', 'administer users');
      // $route->setRequirement('_access', 'FALSE');
      
    }

    // View user
    if ($route = $collection->get('entity.user.canonical')) {
      // $route->setPath('/algo/4040/');
      // $route->setRequirement('_access', 'FALSE');
    }
  }
}