<?php

namespace Drupal\prevent_access\Controller;

use Drupal\Core\Session\AccountInterface;
use Symfony\Component\HttpFoundation\Request;

class RedirectController {  
  
  // ...
  public function content(AccountInterface $user, Request $request) {
    // Do something with $user.
  }
}