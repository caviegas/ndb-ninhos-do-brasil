<?php

namespace Drupal\block_authors\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\node\Entity\Node;
use Drupal\Core\Entity\Query\QueryInterface;
use Drupal\Core\Entity\EntityTypeManager;
use Drupal\user\Entity\User;
use Drupal\file\Entity\File;
use Drupal\media\Entity\Media;
use Drupal\views\Entity\View;
Use \Drupal\taxonomy\Entity\Term;



/**
 * Provides a 'Feed' Block.
 *
 * @Block(
 *   id = "block_authors",
 *   admin_label = "Authors block",
 *   category = "Meeg blocks",
 * )
 */
class FeedBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {


    // Pega a página atual pela ID
    $view_id = \Drupal::routeMatch()->getParameter('view_id');
    if ($view_id) {
      $view = View::load($view_id);
    }
    $isUserView = ($view_id == 'listagem_embaixadores') ? true : false;

    $users = User::loadMultiple();
    $authors = [];

    foreach ($users as $user) {
      if ($user->hasRole('embaixador')) {

        $img = $user->get('uid')->value;

        $user = $user->toArray();
        $userId = $user['uid'][0]['value'];

        // get node ids
        $nids = \Drupal::entityQuery('node')
        ->condition('status', 1)
        // ->condition('type', 'forum')
        ->condition('uid', $userId)
        ->sort('created', 'DESC')
        ->range(0, 1)
        ->execute();
        
        if ($nids != null) {

          // generate picture path
          if ($user['user_picture']) {
            $avatarId = $user['user_picture'][0]['target_id'];
            $file = File::load($avatarId);
            $path = file_create_url($file->get('uri')->value);
          } else {
            $path = null;
          }
            
          // get single node id from the damn returned associative array
          $nids = array_values($nids);
          $nid = $nids[0];
          $node = Node::load($nid);

          $nodeAgeTagId = $node->field_tag_faixaetaria->getValue()[0]['target_id'];
          $term = Term::load($nodeAgeTagId);

          $authors = array_merge($authors, [
            [
              'id' => $userId,
              'name' => $user['name'][0]['value'],
              'picture' => $path,
              'lastPostTitle' => $node->getTitle(),
              'lastPostUrl' => $node->url(),
              'lastPostTag' => $term->label()
              ]
          ]);
        }
      }
    }

    if(!$isUserView) {
      if (count($authors) > 5) {
        $showMore = true;
        $authors = array_slice($authors, 0, 5);
      }
    }

    return [
      '#theme' => 'block_authors',
      '#users' => $authors,
      '#isUserView' => $isUserView,
      '#showMore' => $showMore ?? false,
      '#attached' => [
        'library' => [
          'block_authors/block-view',
        ],
      ]
    ];
  }

}