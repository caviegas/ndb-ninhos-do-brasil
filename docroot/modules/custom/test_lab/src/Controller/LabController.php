<?php

namespace Drupal\test_lab\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\JsonResponse;
use Drupal\Core\Database\DatabaseExceptionWrapper;
use \DOMDocument;
/**
 * Defines LabController class.
 */
class LabController extends ControllerBase {

 
    public function build() {
        
        $webform_id = 'modelo_enquete';

        $query = \Drupal::entityQuery('webform_submission')
            ->accessCheck(FALSE)
            ->condition('webform_id', $webform_id);
        $result = $query->execute();
        $submissionArr = [];
        foreach ($result as $item) {
            $submission = \Drupal\webform\Entity\WebformSubmission::load($item);
            $submissionArr[] = array_values($submission->getData())[0];
        }
        dd($submissionArr);
        return;
    }

}