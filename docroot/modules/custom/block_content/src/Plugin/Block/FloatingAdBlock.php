<?php

namespace Drupal\block_content\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\block_content\Controller\UserController;
use Drupal\block_content\Controller\ContentController;
use Drupal\user_location\Controller\UserLocationController;
use Drupal\node\Entity\Node;
use Drupal\Core\Entity\Query\QueryInterface;
Use \Drupal\taxonomy\Entity\Term;
use Drupal\Core\Entity\EntityTypeManager;
use Drupal\user\Entity\User;
use Drupal\views\Entity\View;


/**
 * Provides a 'Feed' Block.
 *
 * @Block(
 *   id = "block_floating_ad",
 *   admin_label = "Floating ad",
 *   category = "Meeg widgets",
 * )
 */
class FloatingAdBlock extends BlockBase {
  /**
   * {@inheritdoc}
   */

  public function build() {
    
    $contentController = new ContentController;
    $userController = new userController;
    $userLocationController = new UserLocationController;
    
    $logged_in = false;
    $tagList = [];

    // Verifica se usuário está logado pela API
    if ($userController->hasBasicData()) {
      $logged_in = true;

      // Pega os atributos do usuário
      $userAttr = $userController->getUserAttr();

      if ($userController->hasChildren($userAttr)) {
        $activeAgeRange = $userController->getActiveAge($userAttr);
      }

      // Pega id dos termos de interesse
      if ($userController->hasTags($userAttr)) {
        $tagList = $userController->getTags($userAttr['nm_tags']);
      }
    }
    
    $currentNode = \Drupal::routeMatch()->getParameter('node');
    // $fixedAd = false;
    // if ((!$logged_in) || (!$userController->hasChildren($userAttr)) || ($currentNode != null && $currentNode->id() == "todas_faixasetarias")) {
    //   $fixedAd = true;
    // }
    
    if ($currentNode == null || $currentNode->bundle() == "page" || $currentNode->bundle() == "webform") {
      return;
    }

    if ($currentNode->bundle() == 'forum') {
      $forumId = $currentNode->taxonomy_forums->getValue()[0]['target_id'];
      $currentNodeAgeRangeId = $contentController->convertForumToAgeRangeId($forumId);
    } else {
      $currentNodeAgeRangeId = $currentNode->field_tag_faixaetaria->getValue()[0]['target_id'];
    }


    // Verificar se tem anúncio fixo
    if (isset($currentNode->field_fixed_ad) && $currentNode->field_fixed_ad->entity != null) {
      $nodes[] = $currentNode->field_fixed_ad->entity;
    } else {
      // Obter os conteúdos por condições aplicadas
      $nidQuery = \Drupal::entityQuery('node')
      ->condition('status', 1)
      ->condition('type', 'ad')
      ->condition('field_tag_faixaetaria', $currentNodeAgeRangeId, 'IN');

      $nids = $nidQuery
      ->sort('created', 'DESC')
      ->execute();

      $nodes = Node::loadMultiple($nids); 
    }
    $fixedAd = ($currentNodeAgeRangeId == 21) ? true : false;

    $list = [];
    $filteredTags = null;
    foreach ($nodes as $node) {
   
      // Objeto para Array
      $nodeArray = $node->toArray();

      // Get image URL
      $picturePath = $contentController->getNodeImagePath($node);

      // Pegar apenas os campos com o prefixo de field tipo tag
      $nodeTags = array_filter($nodeArray, function($key) {  
          return strpos($key, 'field_tag_') === 0;
      }, ARRAY_FILTER_USE_KEY);
    
      $tags = null;
      // Se o conteúdo tiver tags:
      if ($nodeTags != null) {
      
        // Função pra retornar apenas arrays com conteúdo.
        $filterFunction = function($v){
          return array_filter($v) != array();
        };

        // Chamando a função acima na variavel das tags
        $tags = array_filter($nodeTags, $filterFunction);

        // Cria objeto tags
        $filteredTags = [];
        foreach ($tags as $tagbundle) {
          foreach ($tagbundle as $tag) {
            // Tag info
            $id = $tag['target_id'];
            $term = Term::load($id);
            $vocabulary = $term->bundle();
            
            // Se ainda não existir esta chave, criar. (Usado para evitar sobrescricao de tags do mesmo vocabulario)
            if (!array_key_exists($vocabulary, $filteredTags)) {
              $filteredTags[$vocabulary] = [];
            };

            $filteredTags[$vocabulary] = array_merge($filteredTags[$vocabulary], [
              [
                'id' => $id,
                'name' => $term->getName(),
              ]
            ]);
          }
        }      

        // Object creation
        $list = array_merge($list, [
          [
            'title' => $node->getTitle(),
            'id' => $node->Id(),
            'body' => $node->body->value,
            'type' => $node->getType(),
            // 'tags' => $filteredTags,
            'link' => [
              'url' => $node->field_link->uri,
              'text' => $node->field_link->title
            ],
            'image' => [
              'url' => $picturePath,
              'alt' => null
            ]
          ]
        ]);
      }
    }

    $chosenAd = ($list != []) ? $list[array_rand($list)] : null;

    return [
      '#theme' => 'block_floating_ad',
      '#ad' => $chosenAd,
      '#activeRange' => $activeRange ?? false,
      '#fixedAd' => $fixedAd,
      '#cache' => [
        'max-age' => 0
      ]
    ];
  }
  // public function getCacheMaxAge() {
  //   return 0;
  // } 
}
