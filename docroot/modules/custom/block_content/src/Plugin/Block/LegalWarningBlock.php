<?php

namespace Drupal\block_content\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\block_content\Controller\UserController;
use Drupal\block_content\Controller\ContentController;
use Drupal\node\Entity\Node;
use Drupal\Core\Entity\Query\QueryInterface;
Use \Drupal\taxonomy\Entity\Term;


/**
 * Provides a 'Feed' Block.
 *
 * @Block(
 *   id = "block_legal_warning",
 *   admin_label = "Legal warning block",
 *   category = "Meeg blocks",
 * )
 */
class LegalWarningBlock extends BlockBase {
  /**
   * {@inheritdoc}
   */
  public function build() {

    $userController = new UserController;
    $contentController = new ContentController;
    $active = false;
    $logged_in = false;
    $targetAgeRanges = [13, 11];
    // Verifica se usuário está logado pela API
    if ($userController->hasBasicData()) {
      $logged_in = true;

      // Pega os atributos do usuário
      $userAttr = $userController->getUserAttr();

      if ($userController->hasChildren($userAttr)) {
        $activeAgeRange = $userController->getActiveAge($userAttr);
      }

      // Pega id dos termos de interesse
      if ($userController->hasTags($userAttr)) {
        $tagList = $userController->getTags($userAttr['nm_tags']);
      }
    }

    $altHome = $contentController->isAlternativeHome();

    if ($altHome == '1 - 2' || $altHome == '3 - 4') {
      
      $active = true;
    } else {
      $page = $contentController->getCurrentPage();
      if ($page != null) {
        // Verificar se é a home, se o usuário está logado, tem idade ativa e se essa idade bate com os alvos.
        if ($page->id() == 'frontpage' && $logged_in && isset($activeAgeRange) && in_array($activeAgeRange->id, $targetAgeRanges)) {
          $active = true;
        }
      }
    }

    // Verificar se é um conteúdo
    $currentNode = \Drupal::routeMatch()->getParameter('node');

    if ($currentNode == null || $currentNode->bundle() == "webform") {
      return;
    }

    if ($currentNode != null && $currentNode->bundle() != "page") {
      // Se é TÓPICO
      if ($currentNode->bundle() == "forum") {
        $forumId = $currentNode->taxonomy_forums->getValue()[0]['target_id'];
        $ageRangeId = $contentController->convertForumToAgeRangeId($forumId);
        $ageRange = Term::load($ageRangeId);
        $active = (in_array($ageRange->id(), $targetAgeRanges)) ? true : false;
      } else {
      // Se é ARTIGO
        if (in_array($currentNode->field_tag_faixaetaria->getValue()[0]['target_id'], $targetAgeRanges)) {
          $active = true;
        }
      }
    }

    return [
      '#theme' => 'block_legal_warning',
      '#active' => $active,
    ];
  }
}