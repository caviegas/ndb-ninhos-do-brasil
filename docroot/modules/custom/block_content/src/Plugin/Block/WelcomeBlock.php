<?php

namespace Drupal\block_content\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\block_content\Controller\UserController;
use Drupal\block_content\Controller\ContentController;
use Drupal\node\Entity\Node;
use Drupal\Core\Entity\Query\QueryInterface;
Use \Drupal\taxonomy\Entity\Term;


/**
 * Provides a 'Feed' Block.
 *
 * @Block(
 *   id = "block_welcome",
 *   admin_label = "Welcome block",
 *   category = "Meeg blocks",
 * )
 */
class WelcomeBlock extends BlockBase {
  /**
   * {@inheritdoc}
   */
  public function build() {

    $userController = new UserController;
    $contentController = new ContentController;

    
    $logged_in = false;
    $tagList = [];
    $ageRange = [];
    // Essa variavel se torna verdadeira caso o usuário esteja na home e autenticado
    $mainPage = false;
    if (isset($_SESSION['get_user'])) {
      $userData = $_SESSION['get_attributes'];
      if (isset($_SESSION['get_attributes']['actives'])) {
        $activeAge = $_SESSION['get_attributes']['actives']['age_filter'];
        $ageRange = $userData['age_filters'][$activeAge];
      } else {
        $ageRange = null;
      }
      // Usuário logado pela API
      $logged_in = true;

      // Pega id do age filter

      // Pega id dos termos de interesse
      if (isset($userData['nm_tags']) && $userData['nm_tags'] != 'null') {
        $tagList = $userController->getTags($userData['nm_tags']);
      }
    }

    $page = $contentController->getCurrentPage();
    if ($page->id() != 'frontpage') {
      $ageRange = new \stdClass();
      $ageRange->label = $page->label();
    } else {
      $mainPage = true;
    }

    
    return [
      '#theme' => 'block-welcome',
      '#ageRange' => $ageRange,
      '#mainPage' => $mainPage,
    ];
  }
}
