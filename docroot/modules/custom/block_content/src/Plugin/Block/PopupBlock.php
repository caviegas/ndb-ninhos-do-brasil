<?php

namespace Drupal\block_content\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\block_content\Controller\UserController;
use Drupal\block_content\Controller\ContentController;
use Drupal\node\Entity\Node;
use Drupal\Core\Entity\Query\QueryInterface;
Use \Drupal\taxonomy\Entity\Term;


/**
 * Provides a 'Feed' Block.
 *
 * @Block(
 *   id = "block_popup",
 *   admin_label = "Popup block",
 *   category = "Meeg blocks",
 * )
 */
class PopupBlock extends BlockBase {
  /**
   * {@inheritdoc}
   */
  public function build() {
  

    $ageForm = \Drupal::formBuilder()->getForm('Drupal\block_content\Form\InterestsPopUpForm');
    // $interestForm = \Drupal::formBuilder()->getForm('Drupal\module_nestle_connect\Form\meuCadastroInteresses');

    // $renderArray['form'] = $builtForm;
  
    $forms['faixaetaria'] = $ageForm;
    // $forms['interesses'] = $interestForm;
    
    return [
      '#theme' => 'block_popup',
      '#forms' => $forms
    ];
  }
}
