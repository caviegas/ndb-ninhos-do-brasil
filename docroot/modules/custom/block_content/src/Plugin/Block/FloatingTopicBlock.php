<?php

namespace Drupal\block_content\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\block_content\Controller\UserController;
use Drupal\block_content\Controller\ContentController;
use Drupal\user_location\Controller\UserLocationController;
use Drupal\node\Entity\Node;
use Drupal\Core\Entity\Query\QueryInterface;
Use \Drupal\taxonomy\Entity\Term;
use Drupal\Core\Entity\EntityTypeManager;
use Drupal\user\Entity\User;
use Drupal\views\Entity\View;


/**
 * Provides a 'Feed' Block.
 *
 * @Block(
 *   id = "block_floating_topic",
 *   admin_label = "Floating topic",
 *   category = "Meeg widget",
 * )
 */
class FloatingTopicBlock extends BlockBase {
  /**
   * {@inheritdoc}
   */

  public function build() {
    
    $contentController = new ContentController;
    $userController = new userController;
    $userLocationController = new UserLocationController;
    
    $logged_in = false;
    $tagList = [];
    $ageRange = null;

    // Verifica se usuário está logado pela API
    if ($userController->hasBasicData()) {
      $logged_in = true;

      // Pega os atributos do usuário
      $userAttr = $userController->getUserAttr();

      if ($userController->hasChildren($userAttr)) {
        $activeAgeRange = $userController->getActiveAge($userAttr);
      }

      // Pega id dos termos de interesse
      if ($userController->hasTags($userAttr)) {
        $tagList = $userController->getTags($userAttr['nm_tags']);
      }
    }
    
    $currentNode = \Drupal::routeMatch()->getParameter('node');

    if ($currentNode == null || $currentNode->bundle() == 'forum' || $currentNode->bundle() == "page" || $currentNode->bundle() == "webform") {
      return;
    }

    $currentNodeAgeRangeId = $currentNode->field_tag_faixaetaria->getValue()[0]['target_id'];

    // Organiza filtro para os topicos com a mesma faixa de idade
    $forumTerm = $contentController->convertAgeRangeIdToForum($currentNodeAgeRangeId);

    // Obter os conteúdos por condições aplicadas
    $nidQuery = \Drupal::entityQuery('node')
    ->condition('status', 1)
    ->condition('type', 'forum')
    ->condition('taxonomy_forums', $forumTerm->id(), '=');

    /* 
      Pega as tags do conteúdo visitado num array
      e aplica no filtro.
    */
    $tagArray = $contentController->getNodeTagIds($currentNode);

    $nid = $nidQuery
    ->condition('field_tag_interesses', $tagArray, 'IN')
    ->sort('created', 'DESC')
    ->execute();
     
    /* 
      Se não vier nada com o filtro de interesses,  
      chamar mais uma vez, porém, sem este filtro
      */
    if (empty($nid)) {
      $nidQuery = \Drupal::entityQuery('node')
      ->condition('status', 1)
      ->condition('type', 'forum')
      ->condition('taxonomy_forums', $forumTerm->id(), '=');
      
      $nid = $nidQuery
      ->sort('created', 'DESC')
      ->execute();
    }
   
    $nodes = Node::loadMultiple($nid); 

    $topics = [];
    foreach ($nodes as $node) {
      // Objeto para Array
      $nodeArray = $node->toArray();
        
      // Pegar apenas os campos com o prefixo de field tipo tag
      // $nodeTags = array_filter($nodeArray, function($key) {  
        // return strpos($key, 'field_tag_') === 0;
      // }, ARRAY_FILTER_USE_KEY);

      $author = $node->getOwner();

      $location = $userLocationController->getUserLocation($author->uuid());

      $comment = $node->get('comment_forum');
      $cids = \Drupal::entityQuery('comment')
      ->condition('entity_id', $node->id())
      ->condition('entity_type', 'node')
      ->sort('cid', 'DESC')
      ->execute();

      $pre_username = $author->getDisplayName();
      $hash_divisor = explode('-', $pre_username);
      $cut_name = $hash_divisor[0];

      $forumId = $node->taxonomy_forums->getValue()[0]['target_id'];
      $ageRangeId = $contentController->convertForumToAgeRangeId($forumId);
      $ageRange = Term::load($ageRangeId);
        
        // Object creation
      $topics = array_merge($topics, [
        [
          'title' => $node->getTitle(),
          'type' => $node->getType(),
          'author' => $author->getDisplayName(),
          'author' => $cut_name,
          'location' => $location,
          'commentsCount' => count($cids),
          // 'tags' => $filteredTags,
          'url' => $node->url(),
          'createdTime' => $node->getCreatedTime(),
          'ageRange' => $ageRange->label()
        ]
      ]);
    }

    // Get the most commented topic
    $mostCommentedTopic = array_reduce($topics, function($carry, $item) {
      // se forem iguais, retorna o mais recente
      if ($carry['commentsCount'] == $item['commentsCount']) {
        if ($carry['createdTime'] > $item['createdTime']) {
          return $carry;
        } else {
          return $item;
        }
      }
      return $carry['commentsCount'] < $item['commentsCount'] ? $item : $carry;
    }, $topics[0]);

    // 1618950416
    // 1618950213 - "A criança quer um pet, devo adotar?"
    return [
      '#theme' => 'block_floating_topic',
      '#topic' => $mostCommentedTopic,
    ];
  }
  // public function getCacheMaxAge() {
  //   return 0;
  // } 
}
