<?php

namespace Drupal\block_content\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\node\Entity\Node;
use Drupal\block_content\Controller\UserController;
use Drupal\block_content\Controller\ContentController;
use Drupal\Core\Entity\Query\QueryInterface;
Use \Drupal\taxonomy\Entity\Term;
use Drupal\Core\Entity\EntityTypeManager;
use Drupal\user\Entity\User;
use Drupal\views\Entity\View;

/**
 * Provides a 'Feed' Block.
 *
 * @Block(
 *   id = "block_read_content",
 *   admin_label = "Reads block",
 *   category = "Meeg feeds",
 * )
 */
class ContentsReadFeedBlock extends BlockBase {
  /**
   * {@inheritdoc}
   */
  public function build() {

    $list = [];
    $tagList = [];
    $userController = new UserController;
    $contentController = new ContentController;
    $ageRange = null;

    if (isset($_SESSION['get_user'])) {
      $userData = $_SESSION['get_attributes'];
      
      if (isset($_SESSION["get_attributes"]["actives"])) {

        // Pega id do age filter
        $activeAgeIndex = $_SESSION["get_attributes"]["actives"]["age_filter"];
        $activeAgeRange = $_SESSION["get_attributes"]["age_filters"][$activeAgeIndex];
      }
      
      // Pegar conteúdos já lido pelo usuário
      if (isset($userData['ndb_contents'])) {

        $readContentIds = $userController->getUserReadContents($userData['ndb_contents']);
      } else {
        $readContentIds = null;
      }
      
      if ($readContentIds == null) {
        $nodes = [];
      } else {
        $nodes = Node::loadMultiple($readContentIds);

        $list = [];
        $filteredTags = null;
        foreach ($nodes as $node) {
      
          // Objeto para Array
          $nodeArray = $node->toArray();

          // Pegar apenas os campos com o prefixo de field tipo tag
          $nodeTags = array_filter($nodeArray, function($key) {  
              return strpos($key, 'field_tag_') === 0;
          }, ARRAY_FILTER_USE_KEY);
        
          $tags = null;
          // Se o conteúdo tiver tags:
          if ($nodeTags != null) {
            
            // Função pra retornar apenas arrays com conteúdo.
            $filterFunction = function($v){
              return array_filter($v) != array();
            };

            // Chamando a função acima na variavel das tags
            $tags = array_filter($nodeTags, $filterFunction);

            // Cria objeto tags
            $filteredTags = [];
            foreach ($tags as $tagbundle) {
              foreach ($tagbundle as $tag) {
                // Tag info
                $id = $tag['target_id'];
                $term = Term::load($id);
                $vocabulary = $term->bundle();
                
                // Se ainda não existir esta chave, criar. (Usado para evitar sobrescricao de tags do mesmo vocabulario)
                if (!array_key_exists($vocabulary, $filteredTags)) {
                  $filteredTags[$vocabulary] = [];
                }

                $filteredTags[$vocabulary] = array_merge($filteredTags[$vocabulary], [
                  [
                    'id' => $id,
                    'name' => $term->getName(),
                  ]
                ]);
              }
            }
          }

          // Pega imagem do conteúdo
          $picturePath = $contentController->getNodeImagePath($node);

          // Object creation
          $list = array_merge($list, [
              [
                'title' => $node->getTitle(),
                'type' => $node->getType(),
                'tags' => $filteredTags,
                'url' => $node->url(),
                'picture' => $picturePath
              ]
            ]);
        }
      }

      // Verifica se o bloco esta sendo chamado na página/view de conteúdos lidos
      $isUserView = $contentController->isUserView();

      return [
        '#theme' => 'block_content_read',
        '#nodes' => $list,
        '#isUserView' => $isUserView,
      ];
    }
  }

}