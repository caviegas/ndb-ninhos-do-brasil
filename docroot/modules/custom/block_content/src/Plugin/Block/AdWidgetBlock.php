<?php

namespace Drupal\block_content\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\block_content\Controller\UserController;
use Drupal\block_content\Controller\ContentController;
use Drupal\node\Entity\Node;
use Drupal\Core\Entity\Query\QueryInterface;
Use \Drupal\taxonomy\Entity\Term;
use Drupal\Core\Entity\EntityTypeManager;
use Drupal\user\Entity\User;
use Drupal\file\Entity\File;


/**
 * Provides a 'Feed' Block.
 *
 * @Block(
 *   id = "block_ad",
 *   admin_label = "Ad widget",
 *   category = "Meeg widgets",
 * )
 */
class AdWidgetBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {


    $logged_in = false;
    $tagList = [];
    $userController = new UserController;
    $contentController = new ContentController;
    $ageRange = null;

    if ($userController->hasBasicData()) {
      $logged_in = true;

      // Pega os atributos do usuário
      $userAttr = $userController->getUserAttr();
      
      // Pega id do age filter
      if ($userController->hasChildren($userAttr)) {
        $userAgeRange = $userController->getActiveAge($userAttr);
      } else {
        $userAgeRange = null;
      }
      
      // Pega id dos termos de interesse
      if (isset($userData['nm_tags']) && $userData['nm_tags'] != null) {
        $tagList = $userController->getTags($userData['nm_tags']);
      }
    }

    // Verificar se é a uma interna
    $node = \Drupal::routeMatch()->getParameter('node');
    $isFrontPage = ($node instanceof \Drupal\node\NodeInterface) ? true : false;
    
    // $activeRange = $_SESSION["get_user"]['codigo'] ?? null;
    //Nicolau

    // Obter os conteúdos por condições aplicadas
    $nidQuery = \Drupal::entityQuery('node')
    ->condition('status', 1)
    ->condition('type', 'ad');

    /* 
    Verificar se o usuário está: 
      - ou deslogado 
      - ou logado e sem filhos 
      - ou na home de todas as páginas
    */
    $currentPage = $contentController->getCurrentPage();
    $fixedAd = false;
    if ((!$logged_in) || (!$userController->hasChildren($userAttr)) || ($currentPage != null && $currentPage->id() == "todas_faixasetarias")) {
      $fixedAd = true;
    }
    
    $isAltHome = $contentController->isAlternativeHome();
    if ($isAltHome) {
      $term = \Drupal::entityTypeManager()
        ->getStorage('taxonomy_term')
        ->loadByProperties(['name' => $isAltHome, 'vid' => 'faixa_etaria']);
      $termId = key($term);
      $ageRange = new \stdClass();
      $ageRange->id = $term[$termId]->id();
      $ageRange->label = $term[$termId]->getName();
      $fixedAd = $ageRange->id == "21" ? true : false;
      $activeRange = $ageRange->label;
      $nidQuery->condition('field_tag_faixaetaria', [$ageRange->id], 'IN');
    } elseif ($logged_in == true && $userAgeRange != null) {
      $activeRange = $userAgeRange->label;
      $nidQuery->condition('field_tag_faixaetaria', [$userAgeRange->id], 'IN'); 
    }
    
    $nids = $nidQuery->sort('created', 'DESC')->execute();
    $nodes = Node::loadMultiple($nids);    

    $list = [];
    $filteredTags = null;
    foreach ($nodes as $node) {
   
      // Objeto para Array
      $nodeArray = $node->toArray();

      // Get image URL
      $picturePath = $contentController->getNodeImagePath($node);

      // Pegar apenas os campos com o prefixo de field tipo tag
      $nodeTags = array_filter($nodeArray, function($key) {  
          return strpos($key, 'field_tag_') === 0;
      }, ARRAY_FILTER_USE_KEY);
    
      $tags = null;
      // Se o conteúdo tiver tags:
      if ($nodeTags != null) {
      
        // Função pra retornar apenas arrays com conteúdo.
        $filterFunction = function($v){
          return array_filter($v) != array();
        };

        // Chamando a função acima na variavel das tags
        $tags = array_filter($nodeTags, $filterFunction);

        // Cria objeto tags
        $filteredTags = [];
        foreach ($tags as $tagbundle) {
          foreach ($tagbundle as $tag) {
            // Tag info
            $id = $tag['target_id'];
            $term = Term::load($id);
            $vocabulary = $term->bundle();
            
            // Se ainda não existir esta chave, criar. (Usado para evitar sobrescricao de tags do mesmo vocabulario)
            if (!array_key_exists($vocabulary, $filteredTags)) {
              $filteredTags[$vocabulary] = [];
            };

            $filteredTags[$vocabulary] = array_merge($filteredTags[$vocabulary], [
              [
                'id' => $id,
                'name' => $term->getName(),
              ]
            ]);
          }
        }
      }
      
      // Object creation
      $list = array_merge($list, [
          [
            'title' => $node->getTitle(),
            'body' => $node->body->value,
            'type' => $node->getType(),
            'tags' => $filteredTags,
            'link' => [
              'url' => $node->field_link->uri,
              'text' => $node->field_link->title
            ],
            'image' => [
              'url' => $picturePath,
              'alt' => null
            ]
          ]
        ]);
    }
    
    $chosenAd = ($list != []) ? $list[array_rand($list)] : null;

    return [
      '#theme' => 'block_ad',
      '#ad' => $chosenAd,
      '#isFrontPage' => $isFrontPage,
      '#activeRange' => $activeRange ?? false,
      '#fixedAd' => $fixedAd,
      '#cache' => [
        'max-age' => 0
      ]
    ];
  }

  public function getCacheMaxAge() {
    return 0;
  } 
}

   