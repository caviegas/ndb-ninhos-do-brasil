<?php

namespace Drupal\block_content\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\block_content\Controller\UserController;
use Drupal\block_content\Controller\ContentController;
use Drupal\user_location\Controller\UserLocationController;
use Drupal\node\Entity\Node;
use Drupal\Core\Entity\Query\QueryInterface;
Use \Drupal\taxonomy\Entity\Term;
use Drupal\Core\Entity\EntityTypeManager;
use Drupal\user\Entity\User;
use Drupal\views\Entity\View;

use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a 'Feed' Block.
 *
 * @Block(
 *   id = "block_topics",
 *   admin_label = "Forum topics feed",
 *   category = "Meeg feeds",
 * )
 */
class TopicsFeedBlock extends BlockBase {
  /**
   * {@inheritdoc}
   */

  public function build() {
    
    $contentController = new ContentController;
    $userController = new userController;
    $userLocationController = new UserLocationController;
    
    $logged_in = false;
    $tagList = [];
    $ageRange = null;

    if ($userController->hasBasicData()) {
      // Usuário logado pela API
      $logged_in = true;

      // Pega os atributos do usuário
      $userAttr = $userController->getUserAttr();

      if ($userController->hasChildren($userAttr)) {
        $activeAgeRange = $userController->getActiveAge($userAttr);
      }
      
      // Pega id dos termos de interesse
      if ($userController->hasTags($userAttr)) {
        $tagList = $userController->getTags($userAttr['nm_tags']);
      }
    }
    
    $altHome = $contentController->isAlternativeHome();

    // dd($altHome);
    // Obter os conteúdos por condições aplicadas
    $nidQuery = \Drupal::entityQuery('node')
    ->condition('status', 1)
    // ->condition('taxonomy_forums', 27, '=')
    ->condition('type', 'forum');
        
    // Caso seja uma home alternativa
    if ($altHome != null) {
      $forumAgeRange = $contentController->getForumAgeRange($altHome);
      $nidQuery->condition('taxonomy_forums', [$forumAgeRange->id, 32], 'IN');
    } else {
    // Caso não, então é a home
      if ($logged_in && isset($activeAgeRange) && $activeAgeRange != null) {
        $forumAgeRange = $contentController->getForumAgeRange($activeAgeRange->label);
        $nidQuery->condition('taxonomy_forums', [$forumAgeRange->id, 32], 'IN');
      }
    }

    $nids = $nidQuery->sort('created', 'DESC')->execute();
    $nodes = Node::loadMultiple($nids); 

    $list = [];
    $filteredTags = null;
    foreach ($nodes as $node) {

      
      // Objeto para Array
      $nodeArray = $node->toArray();
      
      // Pegar apenas os campos com o prefixo de field tipo tag
      $nodeTags = array_filter($nodeArray, function($key) {  
        return strpos($key, 'field_tag_') === 0;
      }, ARRAY_FILTER_USE_KEY);
      
      
      $forumId = $node->get('taxonomy_forums')->getValue()[0]['target_id'];
      $ageRangeId = $contentController->convertForumToAgeRangeId($forumId);

      $nodeTags['field_tag_faixaetaria'] = array_merge($nodeTags['field_tag_faixaetaria'] = [
        [
          'target_id' => $ageRangeId
        ]
      ]);

            
      $tags = null;
      
      // Se o conteúdo tiver tags:
      if ($nodeTags != null) {
        
        // Função pra retornar apenas arrays com conteúdo.
        $filterFunction = function($v){
          return array_filter($v) != array();
        };

        // Chamando a função acima na variavel das tags
        $tags = array_filter($nodeTags, $filterFunction);

          // Cria objeto tags
          $filteredTags = [];
          foreach ($tags as $tagbundle) {
            foreach ($tagbundle as $tag) {
              // Tag info
              $id = $tag['target_id'];
              $term = Term::load($id);
              $vocabulary = $term->bundle();
              
              /* 
                Se ainda não existir esta chave, criar. 
                (Usado para evitar sobrescricao de tags do mesmo vocabulario)
              */
              if (!array_key_exists($vocabulary, $filteredTags)) {
                $filteredTags[$vocabulary] = [];
              }

              $filteredTags[$vocabulary] = array_merge($filteredTags[$vocabulary], [
                [
                  'id' => $id,
                  'name' => $term->getName(),
                ]
              ]);
            } // tagbundle foreach
          } // tag as bundle
        }
      $author = $node->getOwner();

      $forum = $node->taxonomy_forums->entity->label();

      $location = $userLocationController->getUserLocation($author->uuid());

      $comment = $node->get('comment_forum');
      $cids = \Drupal::entityQuery('comment')
      ->condition('entity_id', $node->id())
      ->condition('entity_type', 'node')
      ->sort('cid', 'DESC')
      ->execute();

      $pre_username = $author->getDisplayName();
      $hash_divisor = explode('-', $pre_username);
      $cut_name = $hash_divisor[0];
      
      // Object creation
      $list = array_merge($list, [
          [
            'title' => $node->getTitle(),
            'type' => $node->getType(),
            //'author' => $author->getDisplayName(),
            'author' => $cut_name,
            'location' => $location,
            'commentsCount' => count($cids),
            'tags' => $filteredTags,
            'url' => $node->url(),
            'forum' => $forum,
          ]
        ]);
    }

    if (count($list) > 5) {
      $showMore = true;
      $list = array_slice($list, 0, 5);
    }

    return [
      // '#cache' => [
      //   'max-age' => 0
      // ],
      '#theme' => 'block_topics',
      '#nodes' => $list,
      '#showMore' => $showMore ?? false,
      '#forumAgeRangeId' => $forumAgeRange->id ?? false
    ];
  }
  // public function getCacheMaxAge() {
  //   return 0;
  // } 
}
