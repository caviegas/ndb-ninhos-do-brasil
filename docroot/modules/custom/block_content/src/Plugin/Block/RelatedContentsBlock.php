<?php

namespace Drupal\block_content\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\block_content\Controller\UserController;
use Drupal\block_content\Controller\ContentController;
use Drupal\node\Entity\Node;
use Drupal\Core\Entity\Query\QueryInterface;
Use \Drupal\taxonomy\Entity\Term;
use Drupal\Core\Entity\EntityTypeManager;
use Drupal\user\Entity\User;
use Drupal\file\Entity\File;


/**
 * Provides a 'Widget' Block.
 *
 * @Block(
 *   id = "block_related_contents",
 *   admin_label = "Related Contents block",
 *   category = "Meeg blocks",
 * )
 */
class RelatedContentsBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {



    $logged_in = false;
    $tagList = [];
    $userController = new UserController;
    $contentController = new ContentController;
    $ageRange = null;

    // Verifica se usuário está logado pela API
    if ($userController->hasBasicData()) {
      $logged_in = true;

      // Pega os atributos do usuário
      $userAttr = $userController->getUserAttr();

      // if ($userController->hasChildren($userAttr)) {
      //   $activeAgeRange = $userController->getActiveAge($userAttr);
      // }

      // Pega id dos termos de interesse
      if ($userController->hasTags($userAttr)) {
        $tagList = $userController->getTags($userAttr['nm_tags']);
      }

    }

    // Pegar conteúdos já lido pelo usuário
    if ($logged_in && isset($userAttr['ndb_contents']) && $userAttr['ndb_contents'] != 'null') {
      $readContentIds = $userController->getUserReadContents($userAttr['ndb_contents']);
    }

    $currentTime = \Drupal::time()->getCurrentTime();
    $node = \Drupal::routeMatch()->getParameter('node');

    if ($node !== null) {
      if ($node->bundle() == 'article') {
        $nodeAgeRangeId = $node->field_tag_faixaetaria->getValue()[0]['target_id'];

        // Obter os conteúdos por condições aplicadas
        $nidQuery = \Drupal::entityQuery('node')
        ->condition('status', 1)
        ->condition('created', $currentTime, '<')
        ->condition('type', 'article', '=')
        ->condition('field_tag_faixaetaria', $nodeAgeRangeId)
        ->condition('nid', $node->id(), 'NOT IN');


        if ($logged_in) {
          if ($tagList != []) {  
            $nidQuery->condition('field_tag_interesses', $tagList->ids, 'IN');
          }

          // Remove os conteúdos já lidos se houver
          // if ($readContentIds != []) {
          //   $nidQuery->condition('nid', $readContentIds, 'NOT IN');
          // }
        }
        
        $nids = $nidQuery->sort('created', 'DESC')->range(0, 6)->execute();

        // Se não retornar nenhum conteúdo, rodamos a query de novo sem filtrar por interesses
        if ($logged_in && $tagList != [] && $nids == []) {
          $nidQuery = \Drupal::entityQuery('node')
          ->condition('status', 1)
          ->condition('created', $currentTime, '<')
          ->condition('type', 'forum', '=')
          ->condition('taxonomy_forums', $nodeAgeRangeId)
          ->condition('nid', $node->id(), '!=');

          // Remove os conteúdos já lidos se houver
          // if ($readContentIds != []) {
          //   $nidQuery->condition('nid', $readContentIds, 'NOT IN');
          // }

          $nids = $nidQuery->sort('created', 'DESC')->range(0, 5)->range(0, 6)->execute();
        }  
        
        $nodes = Node::loadMultiple($nids);    

        $list = [];
        $filteredTags = null;
        foreach ($nodes as $node) {
        $isRead = false;
        if ($logged_in && $readContentIds != []) {
          $isRead = in_array($node->id(), $readContentIds) ? true : false;
        }

          // Objeto para Array
          $nodeArray = $node->toArray();

          // Pegar apenas os campos com o prefixo de field tipo tag
          $nodeTags = array_filter($nodeArray, function($key) {  
              return strpos($key, 'field_tag_') === 0;
          }, ARRAY_FILTER_USE_KEY);
        
          $tags = null;
          // Se o conteúdo tiver tags:
          if ($nodeTags != null) {
          
            // Função pra retornar apenas arrays com conteúdo.
            $filterFunction = function($v){
              return array_filter($v) != array();
            };

            // Chamando a função acima na variavel das tags
            $tags = array_filter($nodeTags, $filterFunction);

            // Cria objeto tags
            $filteredTags = [];
            foreach ($tags as $tagbundle) {
              foreach ($tagbundle as $tag) {
                // Tag info
                $id = $tag['target_id'];
                $term = Term::load($id);
                $vocabulary = $term->bundle();
                
                // Se ainda não existir esta chave, criar. (Usado para evitar sobrescricao de tags do mesmo vocabulario)
                if (!array_key_exists($vocabulary, $filteredTags)) {
                  $filteredTags[$vocabulary] = [];
                }

                $filteredTags[$vocabulary] = array_merge($filteredTags[$vocabulary], [
                  [
                    'id' => $id,
                    'name' => $term->getName(),
                  ]
                ]);
              }
            }
          }

          // Pega imagem do conteúdo
          $picturePath = $contentController->getNodeImagePath($node);

          // Object creation
          $list = array_merge($list, [
              [
                'title' => $node->getTitle(),
                'type' => $node->getType(),
                'tags' => $filteredTags,
                'url' => $node->url(),
                'read' => $isRead,
                'picture' => $picturePath
              ]
            ]);
        }

        return [
          '#theme' => 'block_related_contents',
          '#nodes' => $list,
          '#cache' => [
            'max-age' => 0
          ]
        ];
      }
    }
  }
}

   