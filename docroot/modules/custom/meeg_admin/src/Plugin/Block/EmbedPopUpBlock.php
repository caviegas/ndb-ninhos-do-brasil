<?php

namespace Drupal\meeg_admin\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Entity\Query\QueryInterface;
use Drupal\Core\Entity\EntityTypeManager;
Use \Drupal\taxonomy\Entity\Term;
use Drupal\node\Entity\Node;
use Drupal\user\Entity\User;
use Drupal\views\Entity\View;


/**
 * Provides a 'Embed Popup' Block.
 *
 * @Block(
 *   id = "embed_popup",
 *   admin_label = "Embed Popup Block",
 *   category = "Meeg feeds",
 * )
 */
class EmbedPopUpBlock extends BlockBase {
  /**
   * {@inheritdoc}
   */
  public function build() {

    $node = \Drupal::routeMatch()->getParameter('node');
    
    $embed_code = '<drupal-entity data-align="center" data-embed-button="node" data-entity-embed-display="view_mode:node.embed" data-entity-type="node" data-entity-uuid="'.$node->uuid->value.'" data-langcode="pt-br"></drupal-entity>';
    
    $data['embed_code'] = $embed_code;

    return [
      '#theme' => 'embed_popup_block',
      '#data' => $data
    ];
  }
}
