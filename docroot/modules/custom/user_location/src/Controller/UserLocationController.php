<?php

namespace Drupal\user_location\Controller;

use Drupal\block_content\Controller\UserController;
use Drupal\Core\Controller\ControllerBase;
use Drupal\views\Entity\View;
Use Drupal\taxonomy\Entity\Term;
use Drupal\file\Entity\File;
use \SoapClient;
use \SoapVar;
use \SoapHeader;
use \SoapFault;
// use Drupal\Core\Database\Connection;
use Drupal\Core\Session\AccountProxy;
use Symfony\Component\HttpFoundation\RequestStack;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Logger\LoggerChannelFactory;
use Symfony\Component\DependencyInjection\ContainerInterface;



/**
 * Defines HelloController class.
 */

class UserLocationController extends ControllerBase {

  // protected $currentUser;
  // protected $database;
  // protected $loggerFactory;

  // public function __construct(
  // AccountProxy $current_user,
  // Connection $database,
  // LoggerChannelFactory $loggerFactory) {
  //   $this->currentUser   = $current_user;
  //   $this->database      = $database;
  //   $this->loggerFactory = $loggerFactory->get('activity_tracking');
  // }

  // /**
  //  * {@inheritdoc}
  //  */
  // public static function create(ContainerInterface $container) {
  //   return new static(
  //     $container->get('current_user'),
  //     $container->get('database'),
  //   );
  // }


  public function setUserLocation($data) {
    $loggerFactory = new LoggerChannelFactory();
    $database = \Drupal::database();

    // Verifica se já existe um registro com esse usuário
    $exists = (bool)$database->select('user_location', 'ul')
    ->condition('ul.uuid', $data['uuid'])
    ->countQuery()->execute()->fetchField();

    if ($exists) {
      $database = db_update('user_location')
      ->fields(array(
        'city' => $data['city'],
        'state' => $data['state'],
        ))
      ->condition('uuid', $data['uuid'])
      ->execute();
    } else {
      $key = [
        'lid' => NULL,
      ];

      $fields = [
        'uuid'  => $data['uuid'],
        'city'  => $data['city'],
        'state'  => $data['state'],
      ];

      try {
        $database->merge('user_location')
          ->key($key)
          ->fields($fields)
          ->execute();
      }
      catch (Exception $e) {
        // Exception handling if something else gets thrown.
        $loggerFactory->error($e->getMessage());
      }
    }
  }

  public function getUserLocation($uuid) {
    $database = \Drupal::database();
    
    $query = $database->select('user_location', 'n');
    $query->condition('n.uuid', $uuid);
    $query->fields('n', ['city', 'state']);
    $result = $query->execute();
    $obj = $result->fetch();

    if ($obj != false) {
      $string = $obj->city.', '.$obj->state;
    } else {
      $string = false;
    }

    return $string;
  }
}

// $userLocationController = new UserLocationController();

// $data = [
//   'uuid' => '131241413asadadad',
//   'city' => 'Porto Alegre',
//   'state' => 'RS'
// ];
// $userLocationController->setUserLocation($data);