<?php
/**
 * @file
 * Contains \Drupal\test_twig\Controller\TestTwigController.
 */
 
namespace Drupal\quiz_submitter\Controller;
 
use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Drupal\webform\Entity\Webform;
use Drupal\webform\Entity\WebformSubmission;

 
class SurveyController extends ControllerBase {
  
  public function submit(Request $request) { 
    $params = \Drupal::request()->request->get('params');

    $webform_id = $params['formId'];
    $webform = Webform::load($webform_id);
    // Create webform submission.
    $values = [
      'webform_id' => $webform->id(),
      'data' => [
        'question' => $params['option'],
      ],
      'uri' => $params['path'],
    ];
    
    /** @var \Drupal\webform\WebformSubmissionInterface $webform_submission */
    $webform_submission = WebformSubmission::create($values);
    $webform_submission->save();

    $keyanswers = $this->getSurveySubmissions($webform);

    return new JsonResponse(json_encode($keyanswers));
  }

  private function getSurveySubmissions($webform) {
    $keyanswers = null;
    $options = $webform->getElementsOriginalDecoded()['question']['#options'];
    $availableOptions = array_keys($options);

    if ($webform->hasSubmissions()) {
      $query = \Drupal::entityQuery('webform_submission')
        ->accessCheck(FALSE)
        ->condition('webform_id', $webform->id());
      $result = $query->execute();
      $submissionArr = [];
      foreach ($result as $item) {
        $submission = \Drupal\webform\Entity\WebformSubmission::load($item);
        $submissionArr[] = array_values($submission->getData())[0];
      }
    
      $votesTotal = count($submissionArr);
      $votesRankingArr = array_count_values($submissionArr);

      foreach ($availableOptions as $index => $key) {
        $value = ($votesRankingArr[$key] == null) ? 0 : $votesRankingArr[$key];
        $percentage = number_format($value * 100 / $votesTotal, 0);
        
        // Adiciona a porcentagem atual dos votos dentro do array
        $keyanswers[] = [
          'option' => $key,
          'votes' => $value,
          'percentage' => $percentage
        ];
      }
      return $keyanswers;
    }

    return false;
  }
}
