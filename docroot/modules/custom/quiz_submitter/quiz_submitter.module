<?php

/**
 * @file
 * Contains quiz_submitter.module.
 */

use \Drupal\user\UserInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Url;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;


/**
 * Implements hook_theme().
 */
function quiz_submitter_theme($existing, $type, $theme, $path) {
  return [
    'quiz__resultado' => [
      'variables' => [
          'data' => null,
      ],
    ],
  ];
}

/*
* Implements hook
*/
function quiz_submitter_form_alter(&$form, FormStateInterface $form_state, $form_id) {
  if (\Drupal::service('router.admin_context')->isAdminRoute()) {
    return;
  }

  if (isset($form['#webform_id']) && strpos($form['#webform_id'], 'quiz_') === 0) {

    $quizUri = \Drupal::request()->getRequestUri();
    $quizUri = preg_replace('/\/pt-br/', '', $quizUri);
    
    
    $form['destination'] = [
      '#type' => 'value',
      '#value' => $quizUri
    ];

    $node = \Drupal::routeMatch()->getParameter('node');
    if ($node != null) {
      $nid = $node->id();
      $form['nid'] = [
        '#type' => 'value',
        '#value' => $nid
      ];
    }

    // Verificar a categoria do quiz antes de mandar pra função adequada.
    $quizCategory = $form_state->getFormObject()->getWebform()->get('category');

    switch ($quizCategory) {
      case 'Profile':
        $form['actions']['submit']['#submit'][] = 'calculateQuizProfile';
        break;
      case 'TrueFalse':
        $form['actions']['submit']['#submit'][] = 'calculateQuizTrueFalse';
        break;
      // case 'Survey':
      //   $form['actions']['submit']['#submit'][] = 'calculateQuizSurvey';
      //   break;
    }
  }

  return;
}

// function calculateQuizSurvey($form, &$form_state) {
//   $inputs = $form_state->getValues();
//   $surveyChoice = $inputs['question_1'];
//   $keyanswers = json_decode($inputs['resultkey'], true);

//   $webformObj = $form_state->getFormObject()->getWebform();
//   // if ($webformObj->hasSubmissions()) {
//   $query = \Drupal::entityQuery('webform_submission')
//     ->condition('webform_id', $webformObj->id());
//   $result = $query->execute();
//   $submission_data = [];
//   foreach ($result as $item) {
//     $submission = \Drupal\webform\Entity\WebformSubmission::load($item);
//     $submission_data[] = array_values($submission->getData())[0];
//   }

  
//   $votesTotal = count($submission_data);
//   $votesRankingArr = array_count_values($submission_data);

//   foreach ($votesRankingArr as $key => $value) {
//     $percentage = number_format($value * 100 / $votesTotal, 0);

//     // Adiciona a porcentagem atual dos votos dentro do array
//     $keyanswers[$key] = array_merge($keyanswers[$key], [
//       'percentage' => $percentage
//     ]);
//   }
//   $data['survey'] = true;
//   $data['title'] = $keyanswers[$surveyChoice]['title'];
//   $data['keyanswers'] = $keyanswers;
  
//   prepareRedirect($data, $inputs, $form_state);
// }

function calculateQuizTrueFalse($form, &$form_state) {
  
  $inputs = $form_state->getValues();
  $keyanswers = json_decode($inputs['resultkey'], true);
  $options = [];

  // Pega só as respostas
  foreach ($inputs as $key => $value) {
    if (strpos($key, 'question_') === 0) {
      $options[$key] = $value;
    }
  }
  
  // Pega a quantidade de questões
  $questionQty = count($options);

  // Pega a quantidade de acertos
  $options = array_values($options);
  $correctAnswers = 0;
  foreach ($options as $option) {
    $correctAnswers += $option;
  }

  // Calcular porcentagem de acerto
  $result = $correctAnswers * 100 / $questionQty;

  // Fazer uma regra de três para saber a porcentagem de acerto
  switch ($result) {
    case ($result >= 0 && $result <= 39):
      $key = "d";
      break;
    case ($result >= 40 && $result <= 69):
      $key = "c";
      break;
    case ($result >= 70 && $result <= 99):
      $key = "b";
      break;
    case ($result == 100):
      $key = "a";
      break;
  }
  
  $data = $keyanswers[$key];

  prepareRedirect($data, $inputs, $form_state);
}


function calculateQuizProfile($form, &$form_state) {

  $inputs = $form_state->getValues();
  $keyanswers = json_decode($inputs['resultkey'], true);

  $options = [];
  // Pega só as respostas
  foreach ($inputs as $key => $value) {
    if (strpos($key, 'question_') === 0) {
      $options[$key] = $value;
    }
  }

  // Pegar somente os valores
  $options = array_values($options);
  // Ver qual opção teve mais respostas
  $options = array_count_values($options);
  arsort($options);
  $options = array_keys($options);
  $result = $options[0];

  $data = $keyanswers[$result];

  prepareRedirect($data, $inputs, $form_state);
}


function prepareRedirect($data, $inputs, $form_state) {
  $data['quiz_title'] = $form_state->getFormObject()->getWebform()->get('title');
  $data['destination'] = $inputs['destination'];
  $data['nid'] = $inputs['nid'];
  
  $session = \Drupal::service('session');
  $session->migrate();
  $session->set('quiz', $data);

  $path = \Drupal\Core\Url::fromRoute('quiz_submitter.result')->toString();
  $response = new RedirectResponse($path);
  $response->send();
}