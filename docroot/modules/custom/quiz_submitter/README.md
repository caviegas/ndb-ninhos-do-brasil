
## Enquete

- Toda a pergunta de um webform do tipo Survey deve contêr a chave de identificação com o valor "question"

- Para embedar o código num artigo, é preciso pegar o id do form e inserir neste formato:
    <survey value="id_do_form">

- Na hora de embedar a tag dentro da área de texto, devemos cuidar para não deixar a tag survey dentro de outra

    Certo:
        <p>Olá</p><survey value="id_form"></survey><p>Muito bem.</p>
    Errado:
        <p>Olá<survey value="id_form"></survey>Muito bem.</p>
