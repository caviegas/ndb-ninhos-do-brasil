(function ($,jQuery) {

    const pathName = window.location.pathname;

  	$(document).ready(function(){
        // const allForms = document.getElementsByTagName('form')
        const formArr = $('form.webform-submission-form');

        // console.log('osform auqi');    
        // console.log(formArr);

        for (i = 0; i < formArr.length; i++) {
            // console.log(formArr[i]);
            $(formArr[i]).find('h4').append('<span></span><strong></strong>')
        }
    })

    // Submittar ao selecionar opção
    $('input:radio').change(function(e){

        const form =  $(this).parents('form');
        form.addClass('voting');
        // $('#loading-image').fadeIn();
        let formId = form.find('#quiz_id').attr('value');    

        let data = {
            path: pathName,
            option: e.target.value,
            formId: formId,
        }

        $.post("/quiz/survey-submit",
            {
                params: data,
            },
            function(data){
                form.removeClass('voting');
                form.addClass('voted');
                data = JSON.parse(data)
                // console.warn('Eis os dados: ')
                // console.log(data)

                for (i = 0; i < data.length; i++) {
                    let input = form.find(`input[value="${data[i].option}"]`)
                    let inputBar = input.next('h4').find('span');
                    let inputText = input.next('h4').find('strong');
                    //inputBar.text(`${data[i].percentage}%`);
                    inputBar.css('width', `${data[i].percentage}%`);
                    if (data[i].votes == 1) {
                        inputText.append(`${data[i].votes} pessoa (${data[i].percentage}%)`);
                    } else {
                        inputText.text(`${data[i].votes} pessoas (${data[i].percentage}%)`);
                    }
                }
            });
    });

})(jQuery);