(function ($,jQuery) {
	$(document).ready(function(){

		if ($(".quiz_resultado")[0]){
			let quizDiv = $(".quiz_resultado");

			let title = quizDiv.find('h4').text() + ' ' + quizDiv.find('h1').text();
			let imageURL = quizDiv.find('img').attr('src');

			$('meta[property="og:title"]').attr('content', title);
			$('meta[property="og:image"]').attr('content', imageURL);
			// $('meta[name="description"]').attr('content', description);
			// $('meta[property="og:description"]').attr('content', description);
		} 

		let submitButton = $(".webform-button--submit:submit");
		let quizForm = submitButton.parents('form:first');
		
		submitButton.click(function() {
			if (quizForm.find('fieldset:not(:has(:radio:checked))').length) {
				allRequired = false
			} else {
				allRequired = true
			}
			if(!allRequired){
				setTimeout(
					function() 
					{
						submitButton.removeClass("focus");
					}
				, 500);
			}
		})
	  
	  //$('article section .section_content .section_content_item .line_01_down h2').each(function (f) {
	  //    var newstr = $(this).text().substring(0,70);
	  //    $(this).text(newstr);
	  //  });


	  	$(".font_a").click(function(){
			$("body").addClass("zoom_one");
			$("body").removeClass("zoom_two");
			$("body").removeClass("zoom_three");
		});
		$(".font_b").click(function(){
			$("body").addClass("zoom_two");
			$("body").removeClass("zoom_one");
			$("body").removeClass("zoom_three");
		});
		$(".font_c").click(function(){
			$("body").addClass("zoom_three");
			$("body").removeClass("zoom_one");
			$("body").removeClass("zoom_two");
		});

	  	$(window).on("scroll", function() {
            if($(window).scrollTop() > 300) {
                $("body").addClass("scrolled");
            } else {
                $("body").removeClass("scrolled");
            }
        });
        $( ".back_top" ).click(function( event ) {
            event.preventDefault();
            $("html, body").animate({ scrollTop: $($(this).attr("href")).offset().top - 0 }, 500);
        });
        
	  	$( ".text-formatted.field a.anchor" ).click(function( event ) {
	        event.preventDefault();
	        $("html, body").animate({ scrollTop: $($(this).attr("href")).offset().top - 100 }, 500);
	    });
	    $( ".accessibility-tools_right .button" ).click(function( event ) {
	        event.preventDefault();
	        $("html, body").animate({ scrollTop: $($(this).attr("href")).offset().top - 120 }, 500);
	    });

    	var classes = ["one", "two", "three", "four"];
	    $(".top-image").each(function(){
	        $(this).addClass(classes[~~(Math.random()*classes.length)]);
	    });

	  	//$('article section .section_content .section_content_item .line_01_down h2').each(function() {
		//    var maxchars = 75;
		//    var seperator = '...';
		//    if ($(this).text().length > (maxchars - seperator.length)) {
		//        $(this).text($(this).text().substr(0, maxchars-seperator.length) + seperator);
		//    }
		//});

		//$('.saved-item .views-field.views-field-title a').each(function() {
		//    var maxchars = 75;
		//    var seperator = '...';
		//    if ($(this).text().length > (maxchars - seperator.length)) {
		//        $(this).text($(this).text().substr(0, maxchars-seperator.length) + seperator);
		//    }
		//});

	})
})(jQuery);

(function ($,jQuery) {

	"use strict";

	var carousels = function () {
		$(".owl-carousel1").owlCarousel({
			loop: false,
			center: false,
			margin: 15,
			responsiveClass: true,
			nav: true,
			dots: false,
			responsive: {
				0: {
					items: 1,
				},
				800: {
					items: 3,
				},
				1300: {
					items: 3,
				},
				1600: {
					items: 4,
				},
				2000: {
					items: 5,
				}
			}
		});
	};

	(function ($) { carousels(); })(jQuery);

})(jQuery);
(function ($,jQuery) {

	"use strict";

	var carousels = function () {
		$(".owl-carousel2").owlCarousel({
			loop: false,
			center: false,
			margin: 15,
			responsiveClass: true,
			nav: true,
			dots: false,
			responsive: {
				0: {
					items: 1,
				},
				800: {
					items: 3,
				},
				1300: {
					items: 3,
				},
				1600: {
					items: 4,
				},
				2000: {
					items: 5,
				}
			}
		});
	};

	(function ($) { carousels(); })(jQuery);

})(jQuery);

(function ($,jQuery) {

	"use strict";

	var carousels = function () {
		$(".owl-carousel3").owlCarousel({
			loop: false,
			center: false,
			margin: 15,
			responsiveClass: true,
			nav: true,
			dots: false,
			responsive: {
				0: {
					items: 1,
				},
				800: {
					items: 3,
				},
				1300: {
					items: 3,
				},
				1600: {
					items: 4,
				},
				2000: {
					items: 5,
				}
			}
		});
	};
	(function ($) { carousels(); })(jQuery);
})(jQuery);

(function ($,jQuery) {

	"use strict";

	var carousels = function () {
		$(".owl-carousel4").owlCarousel({
			loop: false,
			center: false,
			margin: 15,
			responsiveClass: true,
			nav: true,
			dots: false,
			responsive: {
				0: {
					items: 1,
				},
				800: {
					items: 3,
				},
				1300: {
					items: 3,
				},
				1600: {
					items: 4,
				},
				2000: {
					items: 5,
				}
			}
		});
	};
	(function ($) { carousels(); })(jQuery);
})(jQuery);


(function ($,jQuery) {

	$(document).ready(function(){
		$(".hello.logged div div").click(function(){
			$(this).toggleClass("active");
			$('#block-notificationwidgetblock').removeClass("active");
		});
	});

	$(document).ready(function(){
		$(".amb .author-id-text span em").click(function(){
			$(".amb .author-id-text").toggleClass("opened");
		});
	});	

	$(document).ready(function(){
		$(".homevideo").click(function(){
			$("body").addClass("show-video");
			//$('.youtube-video')[0].contentWindow.postMessage('{"event":"command","func":"' + 'playVideo' + '","args":""}', '*');
		});
	});	
	$(document).ready(function(){
		$(".homevideo-popup-close").click(function(){
			$("body").removeClass("show-video");
			$('.youtube-video')[0].contentWindow.postMessage('{"event":"command","func":"' + 'stopVideo' + '","args":""}', '*');
		});
	});	
	
	
	$(document).ready(function(){
		$(".js-form-type-search").click(function(){
			$("body").addClass("search-box-visible");
		});
	});
	$(document).ready(function(){
		$(":submit").click(function(){
			$(this).addClass("focus");
		});
	});
	$(document).ready(function(){
		$(".button").click(function(){
			$(this).addClass("focus");
		});
	});
	$(document).ready(function(){
		$(".search_overlay").click(function(){
			$("body").removeClass("search-box-visible");
		});
	});
	$(document).ready(function(){
		$(".giphy-embed").parent().addClass("giphy-embed_container");
	});
	$(document).ready(function(){
		$(".menu-icon").click(function(){
			$("body").toggleClass("mobile-menu-active");
		});
	});
	$(document).ready(function(){
		$(".search-icon").click(function(){
			$("body").addClass("search-mobile-active");
		});
	});
	$(document).ready(function(){
		$(".close-search").click(function(){
			$("body").removeClass("search-mobile-active");
		});
	});
	$(document).ready(function(){
		$(".save").click(function(){
			$(".save").toggleClass("active");
		});
	});
	$(document).ready(function(){
		$(".share_link").click(function(){
			$(".URLmessage").addClass("show");
		});
	});
	$(document).ready(function(){
		$(".URLmessage").click(function(){
			$(".URLmessage").removeClass("show");
		});
	});
	$(document).ready(function(){
		$(".your_phase_show .down_phase_show").click(function(){
			$(".your_phase_show").toggleClass("active");
		});
	});
	$(document).ready(function(){
		$(".product-detail-header-nutritional").click(function(){
			$(".product-detail-body-nutritional").toggleClass("active");
			$(".product-detail-header-nutritional").toggleClass("active");
		});
	});
		$(document).ready(function(){
		$(".product-detail-header-composition").click(function(){
			$(".product-detail-body-composition").toggleClass("active");
			$(".product-detail-header-composition").toggleClass("active");
		});
	});
	$(document).ready(function(){
		$(".product-detail-header-more").click(function(){
			$(".product-detail-body-more").toggleClass("active");
			$(".product-detail-header-more").toggleClass("active");
		});
	});

	$(document).ready(function(){
		$(".product-detail-header-nutritional").click(function(){
			$(".product-detail-header-composition").removeClass("active");
			$(".product-detail-header-more").removeClass("active");
			$(".product-detail-body-composition").removeClass("active");
			$(".product-detail-body-more").removeClass("active");
		});
	});
		$(document).ready(function(){
		$(".product-detail-header-composition").click(function(){
			$(".product-detail-header-nutritional").removeClass("active");
			$(".product-detail-header-more").removeClass("active");
			$(".product-detail-body-nutritional").removeClass("active");
			$(".product-detail-body-more").removeClass("active");
		});
	});
	$(document).ready(function(){
		$(".product-detail-header-more").click(function(){
			$(".product-detail-header-composition").removeClass("active");
			$(".product-detail-header-nutritional").removeClass("active");
			$(".product-detail-body-composition").removeClass("active");
			$(".product-detail-body-nutritional").removeClass("active");
		});
	});

	$(document).ready(function(){
		$(".horizontal_banner_close").click(function(){
			$("body").toggleClass("banner_disabled");
		});
	});

  	$(document).ready(function(){
	  	$(window).on("scroll", function() {
			if ( screen.width > 1051 ){
	    		if($(window).scrollTop() > 800) {
	        		$("body").addClass("banner");
	    		} else {
	        		$("body").removeClass("banner");
	    		}
	    	}else{
	        	if($(window).scrollTop() > 500) {
	        		$("body").addClass("banner");
	    		} else {
	        		$("body").removeClass("banner");
	    		}
	    	}
	    });
	});

})(jQuery);

function copyURL() {
    //var copyText = document.getElementById("URLinput");
    //copyText.select();
    //copyText.setSelectionRange(0, 99999)
    //document.execCommand("copy");

	var range = document.createRange();
	range.selectNode(document.getElementById("URLinput"));
	window.getSelection().removeAllRanges(); // clear current selection
	window.getSelection().addRange(range); // to select text
	document.execCommand("copy");
	window.getSelection().removeAllRanges();
}
function copyEmbed() {
	var range = document.createRange();
	range.selectNode(document.getElementById("embedInput"));
	window.getSelection().removeAllRanges(); // clear current selection
	window.getSelection().addRange(range); // to select text
	document.execCommand("copy");
	window.getSelection().removeAllRanges();
}

// Text to Speech
if (document.getElementById('voice-controls')){
	const playSpeaker = document.getElementById('start');
	const pauseSpeaker = document.getElementById('pause');
	const resumeSpeaker = document.getElementById('resume');
	const restartSpeaker = document.getElementById('restart');
	const speaker = new SpeechSynthesisUtterance();
	speaker.lang = 'pt';
	let articleArr = [];
	let i = 0;
	speechSynthesis.cancel();
	function organizeText() {
		speechSynthesis.cancel();	
		console.log('Organizando texto no Voice...');
		let articleText = document.getElementsByClassName('field--name-body')[0].innerText;
		articleText = articleText.replaceAll('?','?#DELIMITER#');
        articleText = articleText.replaceAll('.','.#DELIMITER#');
        articleText = articleText.replaceAll('!','!#DELIMITER#');
        articleText = articleText.split('#DELIMITER#') 
		
		articleArr = articleText.filter((value, index) => {
			if (value != "") {
				return value;
			}
		}).flatMap((value, index) => {
			if (value.length > 160) {
				value = value.replaceAll(',',',#DELIMITER#');
				divided = value.split('#DELIMITER#');
				return divided;
			}
			return value;
		}) 

		// Organizar a introdução: Título do artigo e autor
		let titleText = document.querySelector('.field--name-title').innerText + '.';
        let authorText = 'Por: ' + document.querySelector('.field--name-uid .full_name').innerText+'.';

        articleArr.unshift(titleText, authorText);
		speak();
	}
	function speak() {
		speechSynthesis.cancel();
		speaker.text = articleArr[i];
		speechSynthesis.speak(speaker);
	}
	speaker.addEventListener('end', function () {
		i++;
		if (i == articleArr.length) {
			console.log('Voice finalizado')
			playSpeaker.classList.add("active");
			pauseSpeaker.classList.remove("active");
		} else {
			speak();
		}
	})
	playSpeaker.addEventListener("click", organizeText, false);
	resumeSpeaker.addEventListener("click", () => {
		console.log('Voice retomado.')
		speechSynthesis.resume();
	});
	speaker.addEventListener("start", () => {
		playSpeaker.classList.remove("active");
		pauseSpeaker.classList.add("active");
		restartSpeaker.disabled = false;
	})
	// Pause
	pauseSpeaker.addEventListener("click", () => {
		resumeSpeaker.classList.add("active");
		restartSpeaker.classList.add("active");
		pauseSpeaker.classList.remove("active");
		console.log('Voice pausado.')
		speechSynthesis.pause();
	});
	// Resume
	resumeSpeaker.addEventListener("click", () => {
		resumeSpeaker.classList.remove("active");
		pauseSpeaker.classList.add("active");
		speechSynthesis.resume();
	});
	// Começar do início
	restartSpeaker.addEventListener("click", () => {
		resumeSpeaker.classList.remove("active");
		pauseSpeaker.classList.add("active");
		// Stop voice and restart
		speechSynthesis.pause();
		// Restart variables
		i = 0;
		articleArr = [];
		setTimeout(() => {
				organizeText();
		}, 1000);
	});
	// Ao termino
}
  


