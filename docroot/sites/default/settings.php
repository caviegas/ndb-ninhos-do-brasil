<?php

/**
 * Load services definition file.
 */
$settings['container_yamls'][] = __DIR__ . '/services.yml';

/* 2 NEW LINES */
$settings['container_yamls'][] = DRUPAL_ROOT . '/sites/development.services.yml';
$settings['cache']['bins']['render'] = 'cache.backend.null';

/**
 * Include the Pantheon-specific settings file.
 *
 * n.b. The settings.pantheon.php file makes some changes
 *      that affect all envrionments that this site
 *      exists in.  Always include this file, even in
 *      a local development environment, to insure that
 *      the site settings remain consistent.
 */
include __DIR__ . "/settings.pantheon.php";

/**
 * If there is a local settings file, then include it
 */
$local_settings = __DIR__ . "/settings.local.php";
if (file_exists($local_settings)) {
  include $local_settings;
}

$conf['error_level'] = 1;

$settings['trusted_host_patterns'] = [
  '^analytics-74746-nestle-ninho-brazil\.pantheonsite\.io$',
  '^quizzes-74746-nestle-ninho-brazil\.pantheonsite\.io$',
  '^development-74746-nestle-ninho-brazil\.pantheonsite\.io$',
  '^dev-74746-nestle-ninho-brazil\.pantheonsite\.io$',
  '^test-74746-nestle-ninho-brazil\.pantheonsite\.io$',
  '^live-74746-nestle-ninho-brazil\.pantheonsite\.io$',
  '^ninhosdobrasil\.com\.br$',
  '^www\.ninhosdobrasil\.com\.br$',
  '^localhost$',
  '^ninhos.local$',
];

// Verbose error log
$config['system.logging']['error_level'] = 'verbose';


$requestUri = $_SERVER['REQUEST_URI'];

if(preg_match('/user\/([0-9])/', $requestUri)) {
  $redirectHome = true;
} elseif (strpos($requestUri, 'user/register') || strpos($requestUri, 'user/login')) {
 $redirectHome = true;
} else {
  $redirectHome = false;
}

if(preg_match('/estados_cidades\.json$/', $requestUri)) {
  $redirectHome = true;
}

//if ($redirectHome) {
//  global $base_url;
//  $url = $base_url.'/';
//  $response = new Symfony\Component\HttpFoundation\RedirectResponse($url);
//  $response->send(); // don't send the response yourself inside controller and form.
//}

error_reporting(E_ALL);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);
// ini_set('memory_limit', '2048M');


// Fatal error: Allowed memory size of 1073741824 bytes exhausted (tried to allocate 1035997184 bytes) in /Applications/MAMP/htdocs/ninho/modules/custom/block_ad/src/Plugin/Block/FeedBlock.php on line 70